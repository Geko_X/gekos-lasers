#Geko's Lasers

##Overview

This mod adds lasers to Minecraft. Lasers are created with Laserite, a naturally spawning ore, that can be mined with an iron pickaxe or equivalent. Laserite Ore gives off a small amount of light, so it should be easy to find.

Once you have obtained some Laserite, it must be energized before it can be used. To energize Laserite, just throw it into a Thermal Expansion Energetic Infuser.

## Installation

Standard Forge mod install. Chuck the mod in your /mods folder and make sure that Thermal Expansion (and its requirements) are installed.