package com.gekox.gekosLasers.network;

import com.gekox.gekosLasers.items.ItemLaserGun;
import com.gekox.gekosLasers.reference.Key;
import com.gekox.gekosLasers.utility.Color;
import com.gekox.gekosLasers.utility.ColorUtils;
import com.gekox.gekosLasers.utility.LogHelper;
import com.gekox.gekosLasers.utility.NBTHelper;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;
import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class UpdateLaserSizeAndDepth implements IMessage, IMessageHandler<UpdateLaserSizeAndDepth, IMessage> {

	private int keyPressed;
	
	public UpdateLaserSizeAndDepth() {
		
	}
	
	public UpdateLaserSizeAndDepth(Key key) {
		
		if (key == Key.LASER_DEPTH) {
			this.keyPressed = Key.LASER_DEPTH.ordinal();
		}
		
		if (key == Key.LASER_SIZE) {
			this.keyPressed = Key.LASER_SIZE.ordinal();
		}
		
		if (key == Key.UNKNOWN) {
			this.keyPressed = Key.UNKNOWN.ordinal();
		}
		
		System.out.println(keyPressed);
		
	}
	
	@Override
	public IMessage onMessage(UpdateLaserSizeAndDepth message, MessageContext ctx) {
		
		//LogHelper.info(message);
		
		EntityPlayer player = ctx.getServerHandler().playerEntity;
		
		if(player != null && player.getCurrentEquippedItem() != null && player.getCurrentEquippedItem().getItem() instanceof ItemLaserGun) {
			
			ItemLaserGun gun = ((ItemLaserGun) player.getCurrentEquippedItem().getItem());
			
			if(message.keyPressed == Key.LASER_DEPTH.ordinal()) {
				gun.changeNumOfThingsBroken(player.getCurrentEquippedItem());
			}
			
			if(message.keyPressed == Key.LASER_SIZE.ordinal()) {
				gun.changeBlockBreakRadius(player.getCurrentEquippedItem());
			}
			
			int rad = (NBTHelper.getInt(player.getCurrentEquippedItem(), "CurrentRadius") - 1) * 2 + 1;
			
			String msg = String.format("%sDiamter: %d Depth: %d", ColorUtils.CODE_WHITE, rad > 0 ? rad : 0, NBTHelper.getInt(player.getCurrentEquippedItem(), "CurrentDepth"));
			player.addChatMessage(new ChatComponentText(msg));
			
		}
		
		
		return null;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.keyPressed = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.keyPressed);
	}
	
	public int getKeyPressed() {
		return this.keyPressed;
	}
	
	public String toString() {
		return "LaserSizeAndDepth message keyPressed value: " + keyPressed;
	}

}
