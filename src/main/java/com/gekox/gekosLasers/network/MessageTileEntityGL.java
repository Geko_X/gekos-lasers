package com.gekox.gekosLasers.network;

import java.util.UUID;

import net.minecraft.tileentity.TileEntity;

import com.gekox.gekosLasers.blocks.tileEntity.TileEntityGL;

import io.netty.buffer.ByteBuf;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

// Mostly taken from
// https://github.com/pahimar/Equivalent-Exchange-3/blob/master/src/main/java/com/pahimar/ee3/network/message/MessageTileEntityEE.java
//
// Thanks once again, Pahimar!

public class MessageTileEntityGL implements IMessage, IMessageHandler<MessageTileEntityGL, IMessage> {

	public int x, y, z;
	public byte orientation;
	public UUID ownerUUID;
	
	public MessageTileEntityGL() {
		
	}
	
	public MessageTileEntityGL(TileEntityGL gl) {
		
		this.x = gl.xCoord;
		this.y = gl.yCoord;
		this.z = gl.zCoord;
		
		this.orientation = (byte)gl.getOrientation().ordinal();
		this.ownerUUID = gl.getOwnerUUID();
		
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {

		this.x = buf.readInt();
		this.y = buf.readInt();
		this.z = buf.readInt();
		
		this.orientation = buf.readByte();
		
		if(buf.readBoolean()) {
			this.ownerUUID = new UUID(buf.readLong(), buf.readLong());
		}
		else {
			this.ownerUUID = null;
		}

	}

	@Override
	public void toBytes(ByteBuf buf) {

		buf.writeInt(x);
		buf.writeInt(y);
		buf.writeInt(z);
		
		buf.writeByte(orientation);
		
		if(ownerUUID != null) {
			buf.writeBoolean(true);
			buf.writeLong(ownerUUID.getMostSignificantBits());
			buf.writeLong(ownerUUID.getLeastSignificantBits());
		}
		
		else {
			buf.writeBoolean(false);
		}

	}

	@Override
	public IMessage onMessage(MessageTileEntityGL message, MessageContext ctx) {

		TileEntity te = FMLClientHandler.instance().getClient().theWorld.getTileEntity(message.x, message.y, message.z);
		
		//System.out.println(message);
		
		if(te instanceof TileEntityGL) {
			TileEntityGL gl = (TileEntityGL)te;
			
			gl.setOrientation(message.orientation);
			gl.setOwnerUUID(message.ownerUUID);
		}
		
		return null;
		
	}
	
	@Override
	public String toString() {
		return String.format("MessageTileEntityGL:\n\tx: %s\n\ty: %s\n\tz: %s\n\torientation: %s\n\townerUUID: %s", this.x, this.y, this.z, this.orientation, this.ownerUUID);
	}

}
