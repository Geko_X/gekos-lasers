package com.gekox.gekosLasers.network;

import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;

import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

// Sends out and receives updates about a turret
public class UpdateTurretMessage implements IMessage, IMessageHandler<UpdateTurretMessage, IMessage> {

	private int x, y, z;
	private int targetID;
	private int damageMod, accuracyMod, cooldownMod, range;
	
	public UpdateTurretMessage() {
		
	}
	
	public UpdateTurretMessage(int x, int y, int z, int targetID, int damage, int accuracy, int cooldown, int range) {
		
		this.x = x;
		this.y = y;
		this.z = z;
		this.targetID = targetID;
		this.damageMod = damage;
		this.accuracyMod = accuracy;
		this.cooldownMod = cooldown;
		this.range = range;
		
	}
	
	@Override
	public IMessage onMessage(UpdateTurretMessage message, MessageContext ctx) {

		System.out.println(message);
		
		World world = ctx.getServerHandler().playerEntity.worldObj;
		TileEntityTurret turret = (TileEntityTurret)world.getTileEntity(message.getX(), message.getY(), message.getZ());
		
		if(turret != null) {
			turret.changeTarget(message.getTarget());
			turret.setDamageMod(message.getDamage());
			turret.setAccuracyMod(message.getAccuracy());
			turret.setCooldownMod(message.getCooldown());
			turret.range = message.range;
			
			turret.getWorldObj().markBlockForUpdate(turret.xCoord, turret.yCoord, turret.zCoord);
			turret.markDirty();
			
		}
		
		return null;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.x = buf.readInt();
		this.y = buf.readInt();
		this.z = buf.readInt();
		this.targetID = buf.readInt();
		this.damageMod = buf.readInt();
		this.accuracyMod = buf.readInt();
		this.cooldownMod = buf.readInt();
		this.range = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.x);
		buf.writeInt(this.y);
		buf.writeInt(this.z);
		buf.writeInt(this.targetID);
		buf.writeInt(this.damageMod);
		buf.writeInt(this.accuracyMod);
		buf.writeInt(this.cooldownMod);
		buf.writeInt(this.range);
	}
	
	public String toString() {
		return String.format("Turret update event at (%d, %d, %d)\n\tTarget: %d, Damage: %d, Accuracy: %d, Cooldown: %d, Range: %d", this.x, this.y, this.z, this.targetID, this.damageMod, this.accuracyMod, this.cooldownMod, this.range);
	}
	
	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public int getZ() {
		return this.z;
	}

	public int getTarget() {
		return this.targetID;
	}
	
	public int getDamage() {
		return this.damageMod;
	}
	
	public int getAccuracy() {
		return this.accuracyMod;
	}
	
	public int getCooldown() {
		return this.cooldownMod;
	}
	
	public int getRange() {
		return this.range;
	}


}
