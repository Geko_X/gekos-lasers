package com.gekox.gekosLasers.init;

import net.minecraftforge.oredict.OreDictionary;

/**
 * Ore dictionary initializer
 * @author X.Hunt
 *
 */

public class ModOreDict {

	public static final void init() {
		
		// Register with oreDictionary
		OreDictionary.registerOre("oreLaserite", ModBlocks.laserOreBlock);
		OreDictionary.registerOre("gemLaserite", ModItems.itemLaserite);
		OreDictionary.registerOre("blockLaserite", ModBlocks.laserStorageBlock);
		
	}
}
