package com.gekox.gekosLasers.init;

import com.gekox.gekosLasers.utility.LogHelper;
import com.gekox.gekosLasers.world.ModWorldGeneration;

import cpw.mods.fml.common.registry.GameRegistry;

/**
 * Init for mod world gen
 * 
 * @author Xavier Hunt
 *
 */
public class ModWorldGen {
	
	public static void init() {
		LogHelper.info("Registering world generators");
		//GameRegistry.registerWorldGenerator(new ModWorldGeneration(), 0);
	}
	
}
