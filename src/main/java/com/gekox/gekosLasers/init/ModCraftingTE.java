package com.gekox.gekosLasers.init;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;
import cofh.thermalexpansion.block.TEBlocks;
import cofh.thermalexpansion.item.TEItems;
import cofh.thermalexpansion.util.crafting.PulverizerManager;
import cofh.thermalexpansion.util.crafting.TransposerManager;

import com.gekox.gekosLasers.utility.LogHelper;

import cpw.mods.fml.common.registry.GameRegistry;

public class ModCraftingTE {

	public static void init() {
		
		LogHelper.info("Registering ThermalExpansion-based recipes");
		
		// Pulverizer recipes
		PulverizerManager.addOreToDustRecipe(2, new ItemStack(ModBlocks.laserOreBlock), new ItemStack(ModItems.itemLaserite), null, 0);
		
		// Fluid transposer recipes
		TransposerManager.addTEFillRecipe(1000, new ItemStack(Items.diamond), new ItemStack(ModItems.itemLaserite), FluidRegistry.getFluidStack("glowstone", 1000), false);
		TransposerManager.addTEFillRecipe(800, new ItemStack(ModItems.itemLaserite), new ItemStack(ModItems.itemEnergizedLaserite), FluidRegistry.getFluidStack("redstone", 800), false);
		
		// Turret
		//GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModBlocks.turretBlock), new Object[] {"ILI", " M ", "IRI", 'L', ModItems.itemLaserite, 'I', "ingotIron", 'M', TEBlocks.blockFrame, 'R', TEItems.powerCoilGold}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModBlocks.turretBlock), new Object[] {"ILI", " M ", "IRI", 'L', ModItems.itemLaserite, 'I', "ingotIron", 'M', new ItemStack(TEBlocks.blockFrame, 1, 0), 'R', TEItems.powerCoilGold}));
		
		// References to some things
		ItemStack laserReactorLeadstone = new ItemStack(ModItems.itemLaserReactor, 1, 0);
		ItemStack laserReactorHardened = new ItemStack(ModItems.itemLaserReactor, 1, 1);
		ItemStack laserReactorRedstone = new ItemStack(ModItems.itemLaserReactor, 1, 2);
		ItemStack laserReactorResonant = new ItemStack(ModItems.itemLaserReactor, 1, 3);
		
		GameRegistry.addRecipe(new ShapedOreRecipe(laserReactorLeadstone, new Object[] {"IDI", "DED", "ICI", 'I', "ingotLead", 'D', "gemDiamond", 'E', ModItems.itemEnergizedLaserite, 'C', TEItems.capacitorBasic}));
		GameRegistry.addRecipe(new ShapedOreRecipe(laserReactorHardened, new Object[] {"IDI", "DRD", "ICI", 'I', "ingotInvar", 'D', "gemDiamond", 'R', laserReactorLeadstone, 'C', TEItems.capacitorHardened}));
		GameRegistry.addRecipe(new ShapedOreRecipe(laserReactorRedstone, new Object[] {"IDI", "DRD", "ICI", 'I', "ingotElectrum", 'D', "gemDiamond", 'R', laserReactorHardened, 'C', TEItems.capacitorReinforced}));
		GameRegistry.addRecipe(new ShapedOreRecipe(laserReactorResonant, new Object[] {"IDI", "DRD", "ICI", 'I', "ingotEnderium", 'D', "gemDiamond", 'R', laserReactorRedstone, 'C', TEItems.capacitorResonant}));
	
	}
	
}
