package com.gekox.gekosLasers.init;

import net.minecraftforge.client.MinecraftForgeClient;

import com.gekox.gekosLasers.client.render.laser.*;
import com.gekox.gekosLasers.client.renderer.item.*;
import com.gekox.gekosLasers.entity.projectile.laser.*;
import com.gekox.gekosLasers.utility.LogHelper;

import cpw.mods.fml.client.registry.RenderingRegistry;

public class ModRenders {

	public static final void init() {
		LogHelper.info("Registering renderers");
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserBlack.class, new RenderProjectileLaserBlack());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserBlue.class, new RenderProjectileLaserBlue());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserBrown.class, new RenderProjectileLaserBrown());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserCyan.class, new RenderProjectileLaserCyan());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserGray.class, new RenderProjectileLaserGray());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserGreen.class, new RenderProjectileLaserGreen());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserLightBlue.class, new RenderProjectileLaserLightBlue());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserLightGray.class, new RenderProjectileLaserLightGray());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserLime.class, new RenderProjectileLaserLime());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserMagenta.class, new RenderProjectileLaserMagenta());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserOrange.class, new RenderProjectileLaserOrange());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserPink.class, new RenderProjectileLaserPink());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserPurple.class, new RenderProjectileLaserPurple());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserRed.class, new RenderProjectileLaserRed());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserWhite.class, new RenderProjectileLaserWhite());
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaserYellow.class, new RenderProjectileLaserYellow());
		
		RenderingRegistry.registerEntityRenderingHandler(ProjectileLaser.class, new RenderProjectileLaser());
		
		//MinecraftForgeClient.registerItemRenderer(ModItems.itemPolishedLaserite, new RenderPolishedLaserite());
		MinecraftForgeClient.registerItemRenderer(ModItems.itemTier1LaserSword, new RenderLaserSword());
		MinecraftForgeClient.registerItemRenderer(ModItems.itemTier2LaserSword, new RenderLaserSword());
		MinecraftForgeClient.registerItemRenderer(ModItems.itemTier3LaserSword, new RenderLaserSword());
		MinecraftForgeClient.registerItemRenderer(ModItems.itemTier4LaserSword, new RenderLaserSword());
	}
	
}
