package com.gekox.gekosLasers.init;

import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import cofh.thermalfoundation.item.TFItems;

import com.gekox.gekosLasers.utility.LogHelper;

import cpw.mods.fml.common.registry.GameRegistry;

public class ModCraftingNonTE {

	public static void init() {
		
		LogHelper.info("Registering non ThermalExpansion-based recipes");
		
		// Laserite
		GameRegistry.addRecipe(new ShapedOreRecipe(ModItems.itemLaserite, new Object[] {" G ", "GDG", " G ", 'G', "dustGlowstone", 'D', "gemDiamond"}));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModItems.itemEnergizedLaserite, new Object[] {"RRR", "RLR", "RRR", 'R', "dustRedstone", 'L', "gemLaserite"}));
		
		// Turret
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModBlocks.turretBlock), new Object[] {"ILI", " M ", "IRI", 'L', ModItems.itemLaserite, 'I', "ingotIron", 'M', ModBlocks.redstoneCrystalBlockUnlit, 'R', TFItems.gearGold}));

		// References to some things
		ItemStack laserReactorLeadstone = new ItemStack(ModItems.itemLaserReactor, 1, 0);
		ItemStack laserReactorHardened = new ItemStack(ModItems.itemLaserReactor, 1, 1);
		ItemStack laserReactorRedstone = new ItemStack(ModItems.itemLaserReactor, 1, 2);
		ItemStack laserReactorResonant = new ItemStack(ModItems.itemLaserReactor, 1, 3);
		
		GameRegistry.addRecipe(new ShapedOreRecipe(laserReactorLeadstone, new Object[] {"IDI", "DED", "ICI", 'I', "ingotLead", 'D', "gemDiamond", 'E', ModItems.itemEnergizedLaserite, 'C', TFItems.gearLead}));
		GameRegistry.addRecipe(new ShapedOreRecipe(laserReactorHardened, new Object[] {"IDI", "DRD", "ICI", 'I', "ingotInvar", 'D', "gemDiamond", 'R', laserReactorLeadstone, 'C', TFItems.gearInvar}));
		GameRegistry.addRecipe(new ShapedOreRecipe(laserReactorRedstone, new Object[] {"IDI", "DRD", "ICI", 'I', "ingotElectrum", 'D', "gemDiamond", 'R', laserReactorHardened, 'C', TFItems.gearElectrum}));
		GameRegistry.addRecipe(new ShapedOreRecipe(laserReactorResonant, new Object[] {"IDI", "DRD", "ICI", 'I', "ingotEnderium", 'D', "gemDiamond", 'R', laserReactorRedstone, 'C', TFItems.gearEnderium}));
	
		
	}
	
}
