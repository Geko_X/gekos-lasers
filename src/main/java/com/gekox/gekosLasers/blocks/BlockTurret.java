package com.gekox.gekosLasers.blocks;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import cofh.api.block.IDismantleable;
import cofh.api.item.IToolHammer;

import com.gekox.gekosLasers.GekosLasers;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;
import com.gekox.gekosLasers.init.ModBlocks;
import com.gekox.gekosLasers.init.ModItems;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.RenderIDs;
import com.gekox.gekosLasers.utility.Color;
import com.gekox.gekosLasers.utility.LogHelper;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

public class BlockTurret extends BlockTileEntityGL {

	public UUID ownerUUID = null;
	
	public BlockTurret() {
		super();	
		this.setBlockName(Names.Blocks.TURRET_BLOCK);
		this.setHardness(2);
		this.setResistance(10);
		this.setHarvestLevel("pickaxe", 2);		// Iron pick
		//minX, minY, minZ, maxX, maxY, maxZ
		this.setBlockBounds(0, 0, 0, 1, 1, 1);
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
		
		if(!world.isRemote && world.getTileEntity(x, y, z) != null && world.getTileEntity(x, y, z) instanceof TileEntityTurret) {
			TileEntityTurret turret = (TileEntityTurret)world.getTileEntity(x, y, z);
			
			// Check shift right-click with a wrench
			if(player.isSneaking() && player.getHeldItem() != null && player.getHeldItem().getItem() instanceof IToolHammer) {
				this.breakBlock(world, x, y, z, world.getBlock(x, y, z), world.getBlock(x, y, z).getDamageValue(world, x, y, z));
			}
			
			
			else {
				player.openGui(GekosLasers.instance, 0, world, x, y, z);
			}
		}
	
		return true;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		return new TileEntityTurret();
	}
	
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public int getRenderType() {
		return RenderIDs.turret;
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack) {
		
		if(world.getBlock(x, y + 1, z) != Blocks.air)
			return;
		
		super.onBlockPlacedBy(world, x, y, z, entity, stack);
		
		world.setBlock(x, y + 1, z, ModBlocks.turretSphereBlock);
		
		if(entity instanceof EntityPlayer){
			EntityPlayer player = (EntityPlayer) entity;
			TileEntityTurret turret = (TileEntityTurret) world.getTileEntity(x, y, z);
			turret.setOwnerUUID(player);
		}
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
		
		if(!world.isRemote) {
			
			if(world.getTileEntity(x, y, z) instanceof TileEntityTurret) {
				TileEntityTurret turret = (TileEntityTurret)world.getTileEntity(x, y, z);
				
				ItemStack stack = turret.getStackInSlot(0);
				
				if(stack != null) {
					EntityItem item = new EntityItem(world, x, y, z, stack);
					world.spawnEntityInWorld(item);
				}
			}
			
			if(world.getBlock(x, y + 1, z) == ModBlocks.turretSphereBlock) {
				world.setBlockToAir(x, y + 1, z);
			}
		}
			
		super.breakBlock(world, x, y, z, block, meta);
	}
	
	@Override
	public Item getItemDropped(int meta, Random rand, int fortune) {
		return new ItemStack(ModBlocks.turretBlock).getItem();
	}
	
	@Override
	public int quantityDropped(int meta, int fortune, Random rand) {
		return 1;
	}

}
