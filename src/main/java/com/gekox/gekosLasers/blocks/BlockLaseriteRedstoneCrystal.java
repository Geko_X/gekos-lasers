package com.gekox.gekosLasers.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaser;
import com.gekox.gekosLasers.init.ModBlocks;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.utility.LogHelper;

/**
 * Laserite Redstone Crystal block. Emits redstone pulse when hit by a laser entity
 *
 */

public class BlockLaseriteRedstoneCrystal extends BlockGL {

	private int hardness = 1;
	private int resistance = 10;
	
	private int ticksEnabledFor = 20;// 1200;
	
	private boolean isEmitting = false;
	
	public BlockLaseriteRedstoneCrystal(boolean isEmitting) {
		super(Material.rock);
		this.isEmitting = isEmitting;
		
		this.setHardness(hardness);
		this.setResistance(resistance);
		this.setStepSound(soundTypeGlass);
		
		if(this.isEmitting) {
			this.setBlockName(Names.Blocks.LASERITE_REDSTONE_CRYSTAL + "Lit");
		}
		
		else {
			this.setBlockName(Names.Blocks.LASERITE_REDSTONE_CRYSTAL + "Unlit");
		}
	}
	
	@Override
	public int tickRate(World world) {
		return this.ticksEnabledFor;
	}
	
	@Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
		LogHelper.debug(world.getBlock(x, y, z) + " was hit by " + entity);
		
		if(entity instanceof ProjectileLaser && this == ModBlocks.redstoneCrystalBlockUnlit) {
			world.setBlock(x, y, z, ModBlocks.redstoneCrystalBlockLit, world.getBlockMetadata(x, y, z), 3);
			world.scheduleBlockUpdate(x, y, z, this, this.tickRate(world));
			
			this.updateNeighbours(world, x, y, z);
			
			LogHelper.debug("Scheduled tick");
		}
		
		super.onEntityCollidedWithBlock(world, x, y, z, entity);
		
	}
	
	private void updateNeighbours(World world, int x, int y, int z) {	
		LogHelper.debug("Updating neighbours");
		world.notifyBlockChange(x, y, z, this);
	}

	@Override
	public void updateTick(World world, int x, int y, int z, Random random) {
		
		super.updateTick(world, x, y, z, random);
		
		LogHelper.debug("Update tick @:" + this);
		
		if(this == ModBlocks.redstoneCrystalBlockLit) {
			LogHelper.debug("Setting block back to unlit");
			world.setBlock(x, y, z, ModBlocks.redstoneCrystalBlockUnlit, world.getBlockMetadata(x, y, z), 3);
		}
		
		this.updateNeighbours(world, x, y, z);
	}
	
	@Override
	public void onBlockAdded(World world, int x, int y, int z) {
		
		super.onBlockAdded(world, x, y, z);
		
		if(this == ModBlocks.redstoneCrystalBlockLit) {
			world.scheduleBlockUpdate(x, y, z, this, this.tickRate(world));
			this.updateNeighbours(world, x, y, z);
		}
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
		super.breakBlock(world, x, y, z, block, meta);
		this.updateNeighbours(world, x, y, z);
	}
	
	@Override
	public Item getItemDropped(int x, Random y, int z) {
		return (new ItemStack(ModBlocks.redstoneCrystalBlockUnlit)).getItem();
	}
	
	@Override
	public boolean canProvidePower() {
		return true;
	}
	
	@Override
	public int isProvidingWeakPower(IBlockAccess iBlockAccess, int x, int y, int z, int int5) {
		return this.isEmitting ? 15 : 0;
	}
	
	public int isProvidingStrongPower(IBlockAccess iBlockAccess, int x, int y, int z, int int5) {
		return this.isEmitting ? 15 : 0;
	}
}
