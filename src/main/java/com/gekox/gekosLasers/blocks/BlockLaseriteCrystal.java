package com.gekox.gekosLasers.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaser;
import com.gekox.gekosLasers.init.ModBlocks;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.utility.LogHelper;

/**
 * Laserite Crystal block. Emits light for a few seconds when hit by a laser entity
 *
 */

public class BlockLaseriteCrystal extends BlockGL {

	private int hardness = 1;
	private int resistance = 10;
	
	private int ticksEnabledFor = 1200;		// 60 seconds
	
	private boolean isLit = false;
	
	public BlockLaseriteCrystal(boolean isLit) {
		super(Material.rock);
		this.isLit = isLit;
		
		this.setHardness(hardness);
		this.setResistance(resistance);
		this.setStepSound(soundTypeGlass);
		
		if(this.isLit) {
			this.setBlockName(Names.Blocks.LASERITE_CRYSTAL + "Lit");
			this.setLightLevel(1);
		}
		
		else {
			this.setBlockName(Names.Blocks.LASERITE_CRYSTAL + "Unlit");
		}
	}
	
	@Override
	public int tickRate(World world) {
		return this.ticksEnabledFor;
	}
	
	@Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
		LogHelper.debug(world.getBlock(x, y, z) + " was hit by " + entity);
		
		if(entity instanceof ProjectileLaser && this == ModBlocks.crystalBlockUnlit) {
			world.setBlock(x, y, z, ModBlocks.crystalBlockLit, world.getBlockMetadata(x, y, z), 3);
			world.scheduleBlockUpdate(x, y, z, this, this.tickRate(world));
			
			this.updateNeighbours(world, x, y, z);
			
			LogHelper.debug("Scheduled tick");
		}
		
		super.onEntityCollidedWithBlock(world, x, y, z, entity);
		
	}
	
	private void updateNeighbours(World world, int x, int y, int z) {
		LogHelper.debug("Updating neighbours");		
		world.notifyBlockChange(x, y, z, this);
	}

	@Override
	public void updateTick(World world, int x, int y, int z, Random random) {
		
		super.updateTick(world, x, y, z, random);
		
		LogHelper.debug("Update tick @:" + this);
		
		if(this == ModBlocks.crystalBlockLit) {
			LogHelper.debug("Setting block back to unlit");
			world.setBlock(x, y, z, ModBlocks.crystalBlockUnlit, world.getBlockMetadata(x, y, z), 3);
		}
		
		this.updateNeighbours(world, x, y, z);
	}
	
	@Override
	public void onBlockAdded(World world, int x, int y, int z) {
		
		super.onBlockAdded(world, x, y, z);
		
		if(this == ModBlocks.crystalBlockLit) {
			world.scheduleBlockUpdate(x, y, z, this, this.tickRate(world));
			this.updateNeighbours(world, x, y, z);
		}
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
		super.breakBlock(world, x, y, z, block, meta);
		this.updateNeighbours(world, x, y, z);
	}
	
	@Override
	public Item getItemDropped(int x, Random y, int z) {
		return (new ItemStack(ModBlocks.crystalBlockUnlit)).getItem();
	}
	
	@Override
	public boolean hasComparatorInputOverride() {
		return true;
	}
	
	@Override
	public int getComparatorInputOverride(World world, int int1, int int2, int int3, int int4) {
		return this.isLit ? 15 : 0;
	}
}
