package com.gekox.gekosLasers.blocks;

import java.util.Random;

import com.gekox.gekosLasers.GekosLasers;
import com.gekox.gekosLasers.init.ModItems;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.Reference;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

// Storage block for Laserite

public class LaserStorageBlock extends BlockGL {

	private float _hardness = 2.0f;
	private float _resistance = 10.0f;
	private float _lightLevel = 0.5f;

	
	public LaserStorageBlock() {
		super(Material.rock);
		
		this.setBlockName(Names.Blocks.LASERITE_BLOCK);
		
		this.setHardness(_hardness);
		this.setResistance(_resistance);
		this.setLightLevel(_lightLevel);
		this.setHarvestLevel("pickaxe", 2);		// Iron pick
		this.setStepSound(soundTypeGlass);
		
	}

}
