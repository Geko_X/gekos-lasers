package com.gekox.gekosLasers.blocks;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import cofh.api.item.IToolHammer;

import com.gekox.gekosLasers.GekosLasers;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityGL;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityRelay;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaser;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserRed;
import com.gekox.gekosLasers.init.ModBlocks;
import com.gekox.gekosLasers.network.MessageTileEntityGL;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.RenderIDs;
import com.gekox.gekosLasers.utility.Color;
import com.gekox.gekosLasers.utility.LaserUtils;
import com.gekox.gekosLasers.utility.LogHelper;
import com.gekox.gekosLasers.utility.Vector3;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockRelay extends BlockTileEntityGL {

	private int hardness = 1;
	private int resistance = 10;

	public BlockRelay() {
		super(Material.rock);
		
		this.setHardness(hardness);
		this.setResistance(resistance);
		this.setStepSound(soundTypeGlass);
		
		this.setBlockName(Names.Blocks.LASER_RELAY);
	}
	
	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		return new TileEntityRelay();
	}
	
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public int getRenderType() {
		return RenderIDs.turret;
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	
	@Override
	public IIcon getIcon(int side, int meta) {
		return this.icons[side];
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(IBlockAccess world, int x, int y, int z, int blockSide) {
		
		TileEntity te = world.getTileEntity(x, y, z);
		if(te instanceof TileEntityRelay) {
			TileEntityRelay relay = (TileEntityRelay)te;
			
			if(blockSide == relay.getOrientation().ordinal()) {
				return this.icons[0];
			}
			
			return this.icons[1];
		}
		
		return null;
		
	}
	
	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entityLiving, ItemStack itemStack) {
		super.onBlockPlacedBy(world, x, y, z, entityLiving, itemStack);
		if (world.getTileEntity(x, y, z) instanceof TileEntityGL) {
			
			int direction = 0;
			
			int pitch = (int)entityLiving.rotationPitch;
			
			if(pitch > 60) {
				direction = ForgeDirection.UP.ordinal();
			}
			
			else if (pitch < -60) {
				direction = ForgeDirection.DOWN.ordinal();
			}
			
			else {
			
				int facing = (int)(Math.floor(entityLiving.rotationYaw * 4.0F / 360.0F + 0.5D)) & 3;
	
				if (facing == 0) {
					direction = ForgeDirection.NORTH.ordinal();
				}
				else if (facing == 1) {
					direction = ForgeDirection.EAST.ordinal();
				}
				else if (facing == 2) {
					direction = ForgeDirection.SOUTH.ordinal();
				}
				else if (facing == 3) {
					direction = ForgeDirection.WEST.ordinal();
				}
			}

			((TileEntityGL) world.getTileEntity(x, y, z)).setOrientation(direction);
		}
	}
	
	// Called when an entity enters this blockspace
	@Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
	
		if(entity instanceof ProjectileLaser) {
			
			ProjectileLaser laser = (ProjectileLaser)entity;
			
			if(!world.isRemote && world.getTileEntity(x, y, z) != null && world.getTileEntity(x, y, z) instanceof TileEntityRelay) {
				TileEntityRelay relay = (TileEntityRelay)world.getTileEntity(x, y, z);
				
				ItemStack stack = laser.getAttachedItemStack();
				int power = laser.getAttachedPower();
				
				ProjectileLaser newLaser = LaserUtils.copyLaser(laser, LaserUtils.determineLaser(world, laser.getColor()));
				
				relay.shoot(newLaser);
				
			}
		}
		
		super.onEntityCollidedWithBlock(world, x, y, z, entity);
		
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
		
		if(!world.isRemote && world.getTileEntity(x, y, z) != null && world.getTileEntity(x, y, z) instanceof TileEntityRelay) {
			TileEntityRelay relay = (TileEntityRelay)world.getTileEntity(x, y, z);

//			player.addChatComponentMessage(new ChatComponentText("Laser Relay"));
//			player.addChatComponentMessage(new ChatComponentText("======================="));
//			player.addChatComponentMessage(new ChatComponentText("Direction: " + relay.getOrientation()));
//			player.addChatComponentMessage(new ChatComponentText("Energy: " + relay.getEnergyStored(ForgeDirection.UNKNOWN) + "/" + relay.getMaxEnergyStored(ForgeDirection.UNKNOWN)));
//			
			// Right-click with a wrench while sneaking
			if(player.isSneaking() && player.getHeldItem() != null && player.getHeldItem().getItem() instanceof IToolHammer) {
				
				this.breakBlock(world, x, y, z, world.getBlock(x, y, z), world.getBlock(x, y, z).getDamageValue(world, x, y, z));
			}
			
			// Normal right click with a wrench, or sneak right-click with empty hand
			else if ((player.isSneaking() && player.getHeldItem() == null) || (player.getHeldItem() != null && player.getHeldItem().getItem() instanceof IToolHammer)) {
					
				int dir = relay.getOrientation().ordinal() + 1;
				if (dir > 5)
					dir = 0;
				
				relay.setOrientation(dir);
				GekosLasers.network.sendToAll(new MessageTileEntityGL(relay));
					
			}
		}
	
		super.onBlockActivated(world, x, y, z, player, side, hitX, hitY, hitZ);
		return false;
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
		
		if(!world.isRemote) {
			
			if(world.getTileEntity(x, y, z) instanceof TileEntityRelay) {
				TileEntityRelay relay = (TileEntityRelay)world.getTileEntity(x, y, z);
				
				ItemStack stack = relay.getStackInSlot(0);
				
				if(stack != null) {
					EntityItem item = new EntityItem(world, x, y, z, stack);
					world.spawnEntityInWorld(item);
				}
			}
		}
			
		super.breakBlock(world, x, y, z, block, meta);
	}
	
}
