package com.gekox.gekosLasers.blocks;

import cofh.api.block.IDismantleable;

import com.gekox.gekosLasers.blocks.tileEntity.TileEntityGL;
import com.gekox.gekosLasers.creativeTab.CreativeTabGL;
import com.gekox.gekosLasers.reference.Reference;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public abstract class BlockTileEntityGL extends BlockContainer {
	
	public IIcon[] icons = new IIcon[6];
	
	public BlockTileEntityGL(Material material) {
		super(material);
		this.setCreativeTab(CreativeTabGL.GL_TAB);
	}
	
	public BlockTileEntityGL() {
		this(Material.iron);
	}
	
	// Overrides for the name
	@Override
	public String getUnlocalizedName() {
		return String.format("tile.%s%s", Reference.RESOURCE_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister) {
		
//		for(int i = 0; i < 6; i++) {
//			icons[i] = iconRegister.registerIcon(this.getUnlocalizedName().substring(this.getUnlocalizedName().indexOf(".") + 1) + "_" + i);
//		}
		
		blockIcon = iconRegister.registerIcon(String.format("%s", getUnwrappedUnlocalizedName(this.getUnlocalizedName())));
		
	}
	
	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}
	

	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entityLiving, ItemStack itemStack) {
		super.onBlockPlacedBy(world, x, y, z, entityLiving, itemStack);
		if (world.getTileEntity(x, y, z) instanceof TileEntityGL) {
			
			int direction = 0;
			int facing = (int)(Math.floor(entityLiving.rotationYaw * 4.0F / 360.0F + 0.5D)) & 3;

			if (facing == 0) {
				direction = ForgeDirection.NORTH.ordinal();
			}
			else if (facing == 1) {
				direction = ForgeDirection.EAST.ordinal();
			}
			else if (facing == 2) {
				direction = ForgeDirection.SOUTH.ordinal();
			}
			else if (facing == 3) {
				direction = ForgeDirection.WEST.ordinal();
			}

			((TileEntityGL) world.getTileEntity(x, y, z)).setOrientation(direction);
		}
	}

}
