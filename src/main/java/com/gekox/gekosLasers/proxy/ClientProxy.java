package com.gekox.gekosLasers.proxy;

import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;

import com.gekox.gekosLasers.blocks.tileEntity.TileEntityRelay;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurretSphere;
import com.gekox.gekosLasers.client.handler.KeyInputEventHandler;
import com.gekox.gekosLasers.client.renderer.item.ItemRendererRelay;
import com.gekox.gekosLasers.client.renderer.item.ItemRendererTurret;
import com.gekox.gekosLasers.client.renderer.tileEntity.*;
import com.gekox.gekosLasers.client.settings.KeyBindings;
import com.gekox.gekosLasers.init.ModBlocks;
import com.gekox.gekosLasers.init.ModRenders;
import com.gekox.gekosLasers.reference.RenderIDs;
import com.gekox.gekosLasers.utility.LogHelper;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

// Client proxy

public class ClientProxy extends CommonProxy {

	@Override
	public void preInit(FMLPreInitializationEvent e) {
		super.preInit(e);
		
		// Add renderers
		ModRenders.init();
		FMLCommonHandler.instance().bus().register(new KeyInputEventHandler());
		registerKeyBindings();
		
	}
	
	@Override
	public void init(FMLInitializationEvent e) {
		super.init(e);
	}
	
	@Override
	public void postInit(FMLPostInitializationEvent e) {
		super.postInit(e);
	}

	@Override
	public void registerKeyBindings() {
		LogHelper.info("Registering keys");
		ClientRegistry.registerKeyBinding(KeyBindings.laserSize);
		ClientRegistry.registerKeyBinding(KeyBindings.laserDepth);
	}

	@Override
	public void initRenderingAndTextures() {
		
		RenderIDs.turret = RenderingRegistry.getNextAvailableRenderId();
		RenderIDs.turret_sphere = RenderingRegistry.getNextAvailableRenderId();
		RenderIDs.relay = RenderingRegistry.getNextAvailableRenderId();
		
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.turretBlock), new ItemRendererTurret());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.relayBlock), new ItemRendererRelay());
		
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTurret.class, new TileEntityRendererTurret());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTurretSphere.class, new TileEntityRendererTurretSphere());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityRelay.class, new TileEntityRendererRelay());
		
	}
	
}
