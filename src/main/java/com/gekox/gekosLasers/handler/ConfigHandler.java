package com.gekox.gekosLasers.handler;

import java.io.File;

import com.gekox.gekosLasers.reference.ConfigSettings;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.Reference;
import com.gekox.gekosLasers.utility.LogHelper;

import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.common.config.Configuration;

// Handles configurations

// Pahimar's vid:
// https://www.youtube.com/watch?v=OIF98zlBA_M&index=6&list=PLQPiZYWovwmnZlgvbHCbz6TefIgeEiVcj

public class ConfigHandler {

	public static Configuration config;
	
	// Values
	//public static boolean testValue = false;
	
	// Functions
	
	public static void init(File configFile) {
		
		LogHelper.info("Reading config file");
		
		if(config == null) {
		
			config = new Configuration(configFile);
			LogHelper.info("Found the config");
			loadConfig();
		}
	}
	
	// Loads config values
	private static void loadConfig() {
		
		LogHelper.info("Loading config");
		
		// World gen
		ConfigSettings.LASERITE_OREGEN_CHANCE = config.getInt("LaseriteSpawnChance", "WorldGen", 15, 0, 100, "Chances to spawn per chunk. Set to 0 for no spawning");
		
		// Crafting
		ConfigSettings.USE_TE_RECIPES = config.getBoolean("UseTERecipes", "Crafting", true, "If enabled, the mod will use Thermal Expansion -based recipes (if TE is also present)");
		
		// If Mr Torgue is playing
//		ConfigSettings.TORGUE = config.getBoolean("TourgeMode", Configuration.CATEGORY_GENERAL, false, "Enable this is you want to play like Mr Tourge");
		
		// Misc
		ConfigSettings.LASER_SOUNDS = config.getFloat("LaserSounds", Configuration.CATEGORY_GENERAL, 0.5f, 0, 1, "Volume level of lasers");
		ConfigSettings.EXPLOSIONS_RESPECT_GAMERULES = config.getBoolean("LasersRespectMobGriefing", Configuration.CATEGORY_GENERAL, false, "Do explosive lasers respect MobGrifing?");
		
		ConfigSettings.RIFLE_COOLDOWN_MODIFER = config.getFloat("LaserRifleCoolDownModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_RIFLE_COOLDOWN_MODIFER, 0.1f, 10, "Value to modify the cooldown of all laser rifles by, eg: Teir1BaseCooldown * LaserRifleCoolDownModifer");
		ConfigSettings.RIFLE_DAMAGE_MODIFER = config.getFloat("LaserRifleDamageModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_RIFLE_DAMAGE_MODIFER, 0.1f, 10, "Value to modify the damage of all laser rifles by, eg: Teir1BaseDamage * LaserRifleDamageModifer");
		ConfigSettings.RIFLE_ENERGY_MODIFER = config.getFloat("LaserRifleEnergyModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_RIFLE_ENERGY_MODIFER, 0.1f, 10, "Value to modify the RF usage of all laser rifles by, eg: Teir1BaseEnergyUse * LaserRifleEnergyModifer");
		
		ConfigSettings.MINING_COOLDOWN_MODIFER = config.getFloat("PrecisionMiningLaserCoolDownModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_MINING_COOLDOWN_MODIFER, 0.1f, 10, "Value to modify the cooldown of all precision mining lasers by, eg: Teir1BaseCooldown * PrecisionMiningLaserCooldownModifer");
		ConfigSettings.MINING_DAMAGE_MODIFER = config.getFloat("PrecisionMiningLaserDamageModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_MINING_DAMAGE_MODIFER, 0.1f, 10, "Value to modify the damage of all precision mining lasers by, eg: Teir1BaseDamage * PrecisionMiningLaserDamageModifer");
		ConfigSettings.MINING_ENERGY_MODIFER = config.getFloat("PrecisionMiningLaserEnergyModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_MINING_ENERGY_MODIFER, 0.1f, 10, "Value to modify the RF usage of all precision mining lasers by, eg: Teir1BaseEnergyUse * PrecisionMiningLaserEnergyModifer");
		
		ConfigSettings.WIDE_COOLDOWN_MODIFER = config.getFloat("WideMiningLaserCoolDownModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_WIDE_COOLDOWN_MODIFER, 0.1f, 10, "Value to modify the cooldown of all wide mining lasers by, eg: Teir1BaseCooldown * WideMiningLaserCooldownModifer");
		ConfigSettings.WIDE_DAMAGE_MODIFER = config.getFloat("WideMiningLaserDamageModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_WIDE_DAMAGE_MODIFER, 0.1f, 10, "Value to modify the damage of all wide mining lasers by, eg: Teir1BaseDamage * WideMiningLaserDamageModifer");
		ConfigSettings.WIDE_ENERGY_MODIFER = config.getFloat("WideMiningLaserEnergyModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_WIDE_ENERGY_MODIFER, 0.1f, 10, "Value to modify the RF usage of all wide mining lasers by, eg: Teir1BaseEnergyUse * WideMiningLaserEnergyModifer");
		
		ConfigSettings.EXPLOSIVE_COOLDOWN_MODIFER = config.getFloat("ExplosiveMiningLaserCoolDownModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_EXPLOSIVE_COOLDOWN_MODIFER, 0.1f, 10, "Value to modify the cooldown of all explosive mining lasers by, eg: Teir1BaseCooldown * ExplosiveMiningLaserCooldownModifer");
		ConfigSettings.EXPLOSIVE_DAMAGE_MODIFER = config.getFloat("ExplosiveMiningLaserDamageModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_EXPLOSIVE_DAMAGE_MODIFER, 0.1f, 10, "Value to modify the damage of all explosive mining lasers by, eg: Teir1BaseDamage * ExplosiveMiningLaserDamageModifer");
		ConfigSettings.EXPLOSIVE_ENERGY_MODIFER = config.getFloat("ExplosiveMiningLaserEnergyModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_EXPLOSIVE_ENERGY_MODIFER, 0.1f, 10, "Value to modify the RF usage of all explosive mining lasers by, eg: Teir1BaseEnergyUse * ExplosiveMiningLaserEnergyModifer");
		
		ConfigSettings.ENDER_COOLDOWN_MODIFER = config.getFloat("EnderLaserCoolDownModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_ENDER_COOLDOWN_MODIFER, 0.1f, 10, "Value to modify the cooldown of all ender lasers by, eg: Teir1BaseCooldown * EnderLaserCooldownModifer");
		ConfigSettings.ENDER_DAMAGE_MODIFER = config.getFloat("EnderLaserDamageModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_ENDER_DAMAGE_MODIFER, 0.1f, 10, "Value to modify the damage of all ender lasers by, eg: Teir1BaseDamage * EnderLaserDamageModifer");
		ConfigSettings.ENDER_ENERGY_MODIFER = config.getFloat("EnderLaserEnergyModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_ENDER_ENERGY_MODIFER, 0.1f, 10, "Value to modify the RF usage of all ender lasers by, eg: Teir1BaseEnergyUse * EnderLaserEnergyModifer");
		
		ConfigSettings.FIRE_COOLDOWN_MODIFER = config.getFloat("FireLaserCoolDownModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_FIRE_COOLDOWN_MODIFER, 0.1f, 10, "Value to modify the cooldown of all fire lasers by, eg: Teir1BaseCooldown * FireLaserCooldownModifer");
		ConfigSettings.FIRE_DAMAGE_MODIFER = config.getFloat("FireLaserDamageModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_FIRE_DAMAGE_MODIFER, 0.1f, 10, "Value to modify the damage of all fire lasers by, eg: Teir1BaseDamage * FireLaserDamageModifer");
		ConfigSettings.FIRE_ENERGY_MODIFER = config.getFloat("FireLaserEnergyModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_FIRE_ENERGY_MODIFER, 0.1f, 10, "Value to modify the RF usage of all fire lasers by, eg: Teir1BaseEnergyUse * FireLaserEnergyModifer");
		
		ConfigSettings.SWORD_DAMAGE_MODIFER = config.getFloat("SwordDamageModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_SWORD_DAMAGE_MODIFER, 0.1f, 10, "Value to modify the damage of all swords by, eg: Teir1BaseDamage * SwordDamageModifer");
		ConfigSettings.SWORD_REFLECT_DAMAGE_MODIFER = config.getFloat("SwordReflectDamageModifer", Reference.LASER_CONFIG_GENERAL, LaserReference.DEFAULT_SWORD_REFLECT_DAMAGE_MODIFER, 0.1f, 10, "Value to modify the damage of all lasers reflected by a sword");
		
		// Relay settings
		ConfigSettings.RELAY_RF_STORAGE = config.getInt("RelayRFStorage", Reference.RELAY_CONFIG, 100000, 10, 1000000, "How much RF a relay can hold. A relay can discharge 10% of its capacity in each burst.");
		ConfigSettings.RELAY_UPDATE_FREQUENCY = config.getInt("RelayUpdateFrequency", Reference.RELAY_CONFIG, 2, 1, 20, "How many ticks between updates/bursts from a relay");
		
		// Laser settings
		ConfigSettings.TIER_1_MAX_ENERGY = config.getInt("Teir1MaxEnergy", Reference.LASER_CONFIG_TIER_1, LaserReference.DEFAULT_TIER_1_MAX_ENERGY, 1, 1000000, "How much RF this teir of laser holds");
		ConfigSettings.TIER_1_BASE_ENERGY_USE = config.getInt("Teir1BaseEnergyUse", Reference.LASER_CONFIG_TIER_1, LaserReference.DEFAULT_TIER_1_BASE_ENERGY_USE, 0, 100000, "Base RF use per shot");
		ConfigSettings.TIER_1_BASE_COOLDOWN = config.getInt("Teir1BaseCooldown", Reference.LASER_CONFIG_TIER_1, LaserReference.DEFAULT_TIER_1_BASE_COOLDOWN, 1, 10000, "Base number of ticks before this teir of laser can fire. 20 ticks = 1 second");
		ConfigSettings.TIER_1_BASE_DAMAGE = config.getInt("Teir1BaseDamage", Reference.LASER_CONFIG_TIER_1, LaserReference.DEFAULT_TIER_1_BASE_DAMAGE, 0, 100, "Base damage that all lasers in this teir do. 1 damage = 1/2 a heart");
		
		ConfigSettings.TIER_2_MAX_ENERGY = config.getInt("Teir2MaxEnergy", Reference.LASER_CONFIG_TIER_2, LaserReference.DEFAULT_TIER_2_MAX_ENERGY, 1, 1000000, "How much RF this teir of laser holds");
		ConfigSettings.TIER_2_BASE_ENERGY_USE = config.getInt("Teir2BaseEnergyUse", Reference.LASER_CONFIG_TIER_2, LaserReference.DEFAULT_TIER_2_BASE_ENERGY_USE, 0, 100000, "Base RF use per shot");
		ConfigSettings.TIER_2_BASE_COOLDOWN = config.getInt("Teir2BaseCooldown", Reference.LASER_CONFIG_TIER_2, LaserReference.DEFAULT_TIER_2_BASE_COOLDOWN, 1, 10000, "Base number of ticks before this teir of laser can fire. 20 ticks = 1 second");
		ConfigSettings.TIER_2_BASE_DAMAGE = config.getInt("Teir2BaseDamage", Reference.LASER_CONFIG_TIER_2, LaserReference.DEFAULT_TIER_2_BASE_DAMAGE, 0, 100, "Base damage that all lasers in this teir do. 1 damage = 1/2 a heart");
		
		ConfigSettings.TIER_3_MAX_ENERGY = config.getInt("Teir3MaxEnergy", Reference.LASER_CONFIG_TIER_3, LaserReference.DEFAULT_TIER_3_MAX_ENERGY, 1, 1000000, "How much RF this teir of laser holds");
		ConfigSettings.TIER_3_BASE_ENERGY_USE = config.getInt("Teir3BaseEnergyUse", Reference.LASER_CONFIG_TIER_3, LaserReference.DEFAULT_TIER_3_BASE_ENERGY_USE, 0, 100000, "Base RF use per shot");
		ConfigSettings.TIER_3_BASE_COOLDOWN = config.getInt("Teir3BaseCooldown", Reference.LASER_CONFIG_TIER_3, LaserReference.DEFAULT_TIER_3_BASE_COOLDOWN, 1, 10000, "Base number of ticks before this teir of laser can fire. 20 ticks = 1 second");
		ConfigSettings.TIER_3_BASE_DAMAGE = config.getInt("Teir3BaseDamage", Reference.LASER_CONFIG_TIER_3, LaserReference.DEFAULT_TIER_3_BASE_DAMAGE, 0, 100, "Base damage that all lasers in this teir do. 1 damage = 1/2 a heart");
		
		ConfigSettings.TIER_4_MAX_ENERGY = config.getInt("Teir4MaxEnergy", Reference.LASER_CONFIG_TIER_4, LaserReference.DEFAULT_TIER_4_MAX_ENERGY, 1, 1000000, "How much RF this teir of laser holds");
		ConfigSettings.TIER_4_BASE_ENERGY_USE = config.getInt("Teir4BaseEnergyUse", Reference.LASER_CONFIG_TIER_4, LaserReference.DEFAULT_TIER_4_BASE_ENERGY_USE, 0, 100000, "Base RF use per shot");
		ConfigSettings.TIER_4_BASE_COOLDOWN = config.getInt("Teir4BaseCooldown", Reference.LASER_CONFIG_TIER_4, LaserReference.DEFAULT_TIER_4_BASE_COOLDOWN, 1, 10000, "Base number of ticks before this teir of laser can fire. 20 ticks = 1 second");
		ConfigSettings.TIER_4_BASE_DAMAGE = config.getInt("Teir4BaseDamage", Reference.LASER_CONFIG_TIER_4, LaserReference.DEFAULT_TIER_4_BASE_DAMAGE, 0, 100, "Base damage that all lasers in this teir do. 1 damage = 1/2 a heart");
		
		if (config.hasChanged())
			config.save();
	}
	
	@SubscribeEvent
	public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent e) {
		
		// Check for this mod
		if (e.modID.equalsIgnoreCase(Reference.MODID)) {
			
			// Resync the configs
			loadConfig();
			
		}
	}
}
