package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Pink laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserPink extends ProjectileLaser {
	
	public ProjectileLaserPink(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.PINK);
	}
	
	public ProjectileLaserPink(World world) {
		super(world, Color.PINK);
	}
}
