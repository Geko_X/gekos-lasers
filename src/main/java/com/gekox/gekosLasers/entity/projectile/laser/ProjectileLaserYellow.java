package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Yellow laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserYellow extends ProjectileLaser {
	
	public ProjectileLaserYellow(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.YELLOW);
	}
	
	public ProjectileLaserYellow(World world) {
		super(world, Color.YELLOW);
	}
}
