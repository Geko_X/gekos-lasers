package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;

/**
 * White laser
 * 
 * @author X.Hunt
 *
 */;

public class ProjectileLaserWhite extends ProjectileLaser {
	
	public ProjectileLaserWhite(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.WHITE);
	}
	
	public ProjectileLaserWhite(World world) {
		super(world, Color.WHITE);
	}
}
