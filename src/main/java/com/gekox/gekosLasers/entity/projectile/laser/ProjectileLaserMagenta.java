package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Magenta laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserMagenta extends ProjectileLaser {
	
	public ProjectileLaserMagenta(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.MAGENTA);
	}
	
	public ProjectileLaserMagenta(World world) {
		super(world, Color.MAGENTA);
	}
}
