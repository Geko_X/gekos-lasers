package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Orange laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserOrange extends ProjectileLaser {
	
	public ProjectileLaserOrange(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.ORANGE);
	}
	
	public ProjectileLaserOrange(World world) {
		super(world, Color.ORANGE);
	}
}
