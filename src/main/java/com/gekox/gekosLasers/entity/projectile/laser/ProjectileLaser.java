package com.gekox.gekosLasers.entity.projectile.laser;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.event.entity.living.EnderTeleportEvent;
import net.minecraftforge.event.world.BlockEvent;
import cofh.api.energy.IEnergyReceiver;

import com.gekox.gekosLasers.client.particle.EntityGLParticleBaseFX;
import com.gekox.gekosLasers.init.ModBlocks;
import com.gekox.gekosLasers.items.ItemLaserSword;
import com.gekox.gekosLasers.reference.ConfigSettings;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.Reference;
import com.gekox.gekosLasers.utility.Color;
import com.gekox.gekosLasers.utility.LaserDamageSource;
import com.gekox.gekosLasers.utility.LaserUtils;
import com.gekox.gekosLasers.utility.LogHelper;
import com.gekox.gekosLasers.utility.NBTHelper;


/**
 * Abstract laser projectile base. <p>
 * Extend this with a color after the name, eg ProjectileLaserBlue or ProjectileLaserRed
 * 
 * @author Xavier Hunt
 *
 */

public abstract class ProjectileLaser extends EntityThrowable {

	private float speed = 1f;
	
//	private double posX;
//	private double posY;
//	private double posZ;
	
	private float yaw;
	private float pitch;
	
	public int strength = 4;
	private int damage = 10;
	
	private int lifeTime = 0;
	public int maxLifeTime = 50;
	private String lifeTimeTag = "LIFETIME";
	
	public int blockBreakRadius = 1;
	public float explosionSize = 1;
	
	public boolean doExplosionDamage = false;
	public boolean doFireDamage = false;
	public boolean doTeleport = false;
	
	public int burnTime = 2;
	
	public int transparencyPassThrough = 254;
	
	// Number of blocks that can be broken before the laser dies
	public int numOfThingsBroken = 0;
	
	public Block[] dontBreakBlocks = {ModBlocks.crystalBlockLit, ModBlocks.crystalBlockUnlit, ModBlocks.redstoneCrystalBlockLit, ModBlocks.redstoneCrystalBlockUnlit};
	
	private Color color = Color.WHITE;
	
	// Power and item information
	protected boolean isWorkLaser = true;
	protected int attachedPower = 200;
	protected ItemStack attachedItemStack;
	
	// The last block position this laser hit
	private int lastX, lastY, lastZ;
	
	public ProjectileLaser(World world) {
		super(world);
		this.setThrowableHeading(this.motionX, this.motionY, this.motionZ, speed, 0);
	}
	
	public ProjectileLaser(World world, Color c) {
		super(world);
		this.color = c;
		this.setThrowableHeading(this.motionX, this.motionY, this.motionZ, speed, 0);
	}
	
	public ProjectileLaser(World world, EntityLivingBase entityBase, Color c) {
		super(world, entityBase);
		this.color = c;
		this.setThrowableHeading(this.motionX, this.motionY, this.motionZ, speed, 0);
	}

	/**
	 * Use this to set properties about the laser
	 * @param damage
	 * @param strength
	 * @param radius
	 * @param doExpl
	 * @param explSize
	 * @param doFire
	 * @param fireTime
	 * @param doTeleport
	 * @return
	 */
	public ProjectileLaser setupLaser(int damage, int strength, int radius, boolean doExpl, float explSize, boolean doFire, int fireTime, boolean doTeleport) {
		this.damage = damage;
		this.strength = strength;
		this.blockBreakRadius = radius;
		this.doExplosionDamage = doExpl;
		this.explosionSize = explSize;
		this.doFireDamage = doFire;
		this.burnTime = fireTime;
		this.doTeleport = doTeleport;
		
		return this;
	}
	
	/**
	 * Use this to set properties about the laser
	 * @param damage
	 * @param strength
	 * @param radius
	 * @param doExpl
	 * @param explSize
	 * @param doFire
	 * @param fireTime
	 * @return
	 */
	public ProjectileLaser setupLaser(int damage, int strength, int radius, boolean doExpl, float explSize, boolean doFire, int fireTime) {
		this.damage = damage;
		this.strength = strength;
		this.blockBreakRadius = radius;
		this.doExplosionDamage = doExpl;
		this.explosionSize = explSize;
		this.doFireDamage = doFire;
		this.burnTime = fireTime;
		
		return this;
	}
	
	/**
	 * Use this to set properties about the laser. <br>
	 * Sets doFire to false and burnTime to 0
	 * @param damage
	 * @param strength
	 * @param radius
	 * @param doExpl
	 * @param explSize
	 * @return
	 */
	public ProjectileLaser setupLaser(int damage, int strength, int radius, boolean doExpl, float explSize) {
		this.setupLaser(damage, strength, radius, doExpl, explSize, false, 0);
		return this;
	}
	
	/**
	 * Use this to set properties about the laser. <br>
	 * Sets only the player damage, mining strength, and mining radius
	 * @param damage
	 * @param strength
	 * @param radius
	 * @return
	 */
	public ProjectileLaser setupLaser(int damage, int strength, int radius) {
		this.setupLaser(damage, strength, radius, false, 0, false, 0);
		return this;
	}
	
	/**
	 * Use this to set properties about the laser. <br>
	 * Sets player damage, doFire, and burnTime
	 * @param damage
	 * @param doFire
	 * @param fireTime
	 * @return
	 */
	public ProjectileLaser setupLaser(int damage, boolean doFire, int fireTime) {
		this.setupLaser(damage, fireTime, 1, false, 0, doFire, fireTime);
		return this;
	}
	
	/**
	 * Use this to set properties about the laser. <br>
	 * Just sets the damage
	 * @param damage
	 * @return
	 */
	public ProjectileLaser setupLaser(int damage) {
		this.setupLaser(damage, -1, 1, false, 0, false, 0);
		return this;
	}
	
	/**
	 * Sets the color of the laser
	 * @param color
	 * @return
	 */
	public ProjectileLaser setColor(Color color) {
		this.color = color;
		return this;
	}
	
	public Color getColor() {
		return this.color;
	}
	
	/**
	 * Sets the speed of the laser
	 * @param speed
	 * @return
	 */
	public ProjectileLaser setLaserSpeed(float speed) {
		this.speed = speed;
		return this;
	}
	
	// Explosions
	public void setExplosion(boolean doExpl) {
		this.doExplosionDamage = doExpl;
	}
	
	public void setExplosion(boolean doExpl, float radius) {
		this.doExplosionDamage = doExpl;
		this.explosionSize = radius;
	}
	
	// Fire
	public void setFire(boolean doFire) {
		this.doFireDamage = doFire;
	}
	
	public void setFire(boolean doFire, int time) {
		this.doFireDamage = doFire;
		this.burnTime = time;
	}
	
	// Block radius
	public void setBlockBreakRadius(int amount) {
		this.blockBreakRadius = amount;
	}
	
	/**
	 * Sets the laser to teleport the shooter to where it hits
	 * <br>
	 * Note that this will hit transparent blocks
	 * @return
	 */
	public ProjectileLaser setTeleport() {
		this.doTeleport = true;
		return this;
	}
	
	/**
	 * Sets the laser to a work laser, and attaches some power
	 */
	public ProjectileLaser attachPower(int power) {
		if(power > 0) {
			this.isWorkLaser = true;
			this.attachedPower = power;
		}
		return this;
	}
	
	/**
	 * Sets the laser to a work laser, and attaches an item
	 */
	public ProjectileLaser attachItemStack(ItemStack stack) {
		if(stack != null) {
			this.isWorkLaser = true;
			this.attachedItemStack = stack;
		}
		return this;
	}
	
	public boolean getIsWorkLaser() {
		return this.isWorkLaser;
	}
	
	public int getAttachedPower() {
		return this.attachedPower;
	}
	
	public ItemStack getAttachedItemStack() {
		return this.attachedItemStack;
	}
	
	// Pass through things
//	public void setPassThroughEntites(boolean passThrough, int amount) {
//		this.passThroughEntities = passThrough;
//		this.passThroughAmount = amount;
//	}
//	
//	public void setPassThroughBlocks(boolean passThrough, int amount) {
//		this.passThroughBlocks = passThrough;
//		this.passThroughAmount = amount;
//	}

	@Override
	public void onImpact(MovingObjectPosition movingObjPos) {
		
		if(this.ticksExisted <= 1) {
			return;
		}
		
		int bX = movingObjPos.blockX;
		int bY = movingObjPos.blockY;
		int bZ = movingObjPos.blockZ;
		
		//System.out.println(bX + " " + bY + " " + bZ);

		World world = this.worldObj;
		int side = movingObjPos.sideHit;
		
		boolean canKill = true;
		
		// If an entity is hit
		if (movingObjPos.entityHit != null) {
			Entity entity = movingObjPos.entityHit;
			
			//LogHelper.info(entity);
			
			if(entity instanceof EntityLiving) {
				EntityLiving entityliving = (EntityLiving)entity;
				
				// Check that this entity is a player, and is holding a lasersword, and is blocking, and the sword is active
				// This is also stupidly long
				
				if((entity != null) && (entity instanceof EntityPlayerMP) && (((EntityPlayerMP)entity).getCurrentEquippedItem() != null) && (((EntityPlayerMP)entity).getCurrentEquippedItem().getItem() instanceof ItemLaserSword) && (((ItemLaserSword)((EntityPlayerMP)entity).getCurrentEquippedItem().getItem()).getIsActive(((EntityPlayerMP)entity).getCurrentEquippedItem())) && ((EntityPlayerMP)entity).isBlocking()) {
					
					LogHelper.debug("Reflecting");
					
					ItemStack stack = ((EntityPlayerMP)entity).getCurrentEquippedItem();
					ItemLaserSword sword = (ItemLaserSword)stack.getItem();
					Color color = Color.color[NBTHelper.getInt(stack, LaserReference.LASER_COLOR_TAG)];
					
					ProjectileLaser laser = LaserUtils.determineLaser(worldObj, (EntityPlayer) entity, color);
					
					laser.setupLaser((int) (this.damage * ConfigSettings.SWORD_REFLECT_DAMAGE_MODIFER), this.strength, this.blockBreakRadius, this.doExplosionDamage, this.explosionSize, this.doFireDamage, this.burnTime);
					laser.numOfThingsBroken = this.numOfThingsBroken;
					
					Random random = new Random();
					
					float varianceX = random.nextFloat() * 1;
					varianceX *= random.nextBoolean() ? 1 : -1;
					
					float varianceY = random.nextFloat() * 1;
					varianceY *= random.nextBoolean() ? 1 : -1; 
					
					float varianceZ = random.nextFloat() * 1;
					varianceZ *= random.nextBoolean() ? 1 : -1;
					
					laser.motionX += varianceX;
					laser.motionY += varianceX;
					laser.motionZ += varianceX;
					laser.setThrowableHeading(laser.motionX, laser.motionY, laser.motionZ, laser.speed, 0);
					
					worldObj.spawnEntityInWorld(laser);
					worldObj.playSoundAtEntity(entity, Reference.RESOURCE_PREFIX + "laser.pew", 0.5f, 1);
					this.setDead();
					
				}
				
				else {
					// Apply damage
					DamageSource ds = new LaserDamageSource(this, this.getThrower());
					entityliving.attackEntityFrom(ds, this.getDamage());
					
					// Set the thing on fire if fire is enabled
					if(this.doFireDamage) {
						entity.setFire(this.burnTime);
					}
					
					// EXPLOSIONS :D
					if(this.doExplosionDamage) {
						world.newExplosion(this, bX, bY, bZ, explosionSize, false, ConfigSettings.EXPLOSIONS_RESPECT_GAMERULES ? (this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing") ? true : false) : true);
					}
					
					this.numOfThingsBroken--;
				}
			}
			
//			if (entity instanceof ProjectileLaser) {
//				
//				LogHelper.info("Hit another laser");
//				
//				if(ConfigSettings.TORGUE) {
//					
//					world.newExplosion(this, bX, bY, bZ, 4, false, true);
//					entity.setDead();
//					this.setDead();
//				}
//			}
			
		}
		
		// ===================================== SERVER =====================================
		
		
		// Server - do all block breaking and entity hurting
		if(!worldObj.isRemote) {
			
//			LogHelper.info(String.format("Hit a thing! (%d, %d, %d)", bX, bY, bZ));
//			LogHelper.info(String.format("%d more things to break", this.numOfThingsBroken));
			
			if(bX == lastX && bY == lastY && bZ == lastZ) {
				//LogHelper.info("Hit this block already! Returning!");
				return;
			}
			
			lastX = bX;
			lastY = bY;
			lastZ = bZ;

			// If a block is hit
			if(movingObjPos.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK) {
				
				Block hitBlock = world.getBlock(bX, bY, bZ);
				
				// Trigger the onEntityCollidedWithBlock method on the block
				hitBlock.onEntityCollidedWithBlock(worldObj, bX, bY, bZ, this);
				
				// ===================================== TELEPORT =====================================
				
				// Teleport the player if this is a teleport laser. Code is straight from enderpearl logic
				if(this.doTeleport) {
					if (this.getThrower() != null && this.getThrower() instanceof EntityPlayerMP) {
						EntityPlayerMP player = (EntityPlayerMP)this.getThrower();

						if (player.playerNetServerHandler.func_147362_b().isChannelOpen() && player.worldObj == this.worldObj)
						{
							EnderTeleportEvent event = new EnderTeleportEvent(player, bX, bY + 1, bZ, 2.0F);
							if (!MinecraftForge.EVENT_BUS.post(event)) {
								
								if (this.getThrower().isRiding()) {
									this.getThrower().mountEntity((Entity)null);
								}

								this.getThrower().setPositionAndUpdate(event.targetX, event.targetY, event.targetZ);
								this.getThrower().fallDistance = 0.0F;
								this.getThrower().attackEntityFrom(DamageSource.fall, event.attackDamage);
							}
						}
					}
				}
				
				// ====================================== WORK LASER ======================================
				
				if(this.isWorkLaser && (world.getTileEntity(bX, bY, bZ) instanceof IInventory || world.getTileEntity(bX, bY, bZ) instanceof IEnergyReceiver)) {
					
					// Check if we hit an inventory 
					if(world.getTileEntity(bX, bY, bZ) instanceof IInventory && this.getAttachedItemStack() != null) {
						IInventory te = (IInventory)world.getTileEntity(bX, bY, bZ);
						
						ItemStack thisStack = this.getAttachedItemStack();
						
						outerloop:
						for(int s = 0; s < te.getSizeInventory(); s++) {
							
							ItemStack stack = te.getStackInSlot(s);
							
							// Check if slot is empty
							if(te.getStackInSlot(s) == null) {
								te.setInventorySlotContents(s, this.getAttachedItemStack());
								this.attachItemStack(null);
								break outerloop;
							}
							
							else if (stack.getItem() == thisStack.getItem()) {
								for(int j = thisStack.stackSize; j > 0; j--) {
									
									if(stack.stackSize < stack.getMaxStackSize()) {
									
										stack.stackSize++;
										thisStack.stackSize--;
									
										if(thisStack.stackSize == 0) {
											this.attachItemStack(null);
											break outerloop;
										}
									}
								}
							}
						}
						
						
						
						// Out of items to send
						if(thisStack.stackSize <= 0 || this.getAttachedItemStack() == null) {
							this.setDead(true);
						}
						
						else {
							this.setDead();
						}
					}
					
					// Check if we hit a powered thing
					if(world.getTileEntity(bX, bY, bZ) instanceof IEnergyReceiver) {
						IEnergyReceiver te = (IEnergyReceiver)world.getTileEntity(bX, bY, bZ);
						
						// Determine the energy to pass this thing
						int powerToPass = Math.min(this.getAttachedPower() / Math.max(this.numOfThingsBroken, 1), te.getMaxEnergyStored(ForgeDirection.NORTH) - te.getEnergyStored(ForgeDirection.NORTH));
						te.receiveEnergy(ForgeDirection.getOrientation(side), powerToPass , false);
						this.attachedPower = Math.max(this.attachedPower - powerToPass, 0);
						
						if(powerToPass > 0) {
						//	LogHelper.info(String.format("Depositied %dRF", powerToPass));
							this.numOfThingsBroken--;
						}
					}
					
				//	this.numOfThingsBroken--;
					
				}
				
				
				
				// ===================================== NON BREAKABLE =====================================
				
				// First, check that this block is NOT in the list of blocks that cant be broken, such as Laserite Crystals
				for(Block b : this.dontBreakBlocks) {
					if(b == hitBlock) {
						
					//	LogHelper.info("Hit a block we cant break");
						
						// Kill the laser immediately
						this.numOfThingsBroken = -1;
						this.setDead();
						return;
					}
				}
				
				// ===================================== FIRE =====================================
				
				// Set fire to things that we pass through
				if(this.doFireDamage && this.getThrower() != null && this.getThrower() instanceof EntityPlayerMP) {
					EntityPlayerMP player = (EntityPlayerMP)this.getThrower();
					setFire(worldObj, bX, bY, bZ, side);
					
				}
				
				// ===================================== NORMAL BLOCKS =====================================
				
				// Do a check here for colored glass. If we hit it, we're gonna change colors
				// But not yet! ;)
//				if(hitBlock == Blocks.stained_glass || hitBlock == Blocks.stained_glass_pane) {
//					
//					int meta = hitBlock.getDamageValue(worldObj, bX, bY, bZ);
//					LogHelper.info("Hit stained glass: " + meta);
//					
//					ProjectileLaser newLaser = LaserUtils.copyLaser(this, LaserUtils.determineLaser(worldObj, Color.color[meta]));
//					newLaser.setPositionAndRotation(posX, posY, posZ, yaw, pitch);
//					newLaser.setThrowableHeading(motionX, motionY, motionZ, 0, this.speed);
//					newLaser.motionX = motionX;
//					newLaser.motionY = motionY;
//					newLaser.motionZ = motionZ;
//					newLaser.speed = speed;
//					
//					worldObj.spawnEntityInWorld(newLaser);
//					this.setDead(true);
//					
//				}
				
				// Check if we can break anything
				if(this.numOfThingsBroken < 0 ){
					this.setDead();
				}
				
				// This is literally the longest if-statement I've ever written
				// Maybe
				if((hitBlock.getLightOpacity() <= this.transparencyPassThrough) || (!hitBlock.isOpaqueCube()) || (hitBlock.isFoliage(world, bX, bY, bZ)) || (hitBlock.hasTileEntity(hitBlock.getDamageValue(worldObj, bX, bY, bZ)))) {
					// No OP
				}
				
				else {
					this.numOfThingsBroken--;
				}
				
				// Try to break all blocks in the radius
				//
				// This method is thanks to SlimeKnights, 
				// https://github.com/SlimeKnights/TinkersConstruct/blob/master/src/main/java/tconstruct/library/tools/AOEHarvestTool.java
				
				//Bottom = 0, Top = 1, East = 2, West = 3, North = 4, South = 5.
				
				int xRange = this.blockBreakRadius;
				int yRange = this.blockBreakRadius;
				int zRange = 1;
				
				switch (side) {
				case 0:
				case 1:
					yRange = 1;
					zRange = this.blockBreakRadius;
					break;
				case 2:
				case 3:
					xRange = this.blockBreakRadius;
					zRange = 1;
					break;
				case 4:
				case 5:
					xRange = 1;
					zRange = this.blockBreakRadius;
					break;
				}
				
				for (int xPos = bX - xRange + 1; xPos <= bX + xRange - 1; xPos++) {
					for (int yPos = bY - yRange + 1; yPos <= bY + yRange - 1; yPos++) {
						for (int zPos = bZ - zRange + 1; zPos <= bZ + zRange - 1; zPos++) {
							
							Block block = world.getBlock(xPos, yPos, zPos);
							int meta = world.getBlockMetadata(xPos, yPos, zPos);
				
							boolean canHit = true;
							
							// First, check the transparency of the block we're hitting
							if(block.getLightOpacity() <= this.transparencyPassThrough) {
								canHit = false;
							}
							
							// Check if a solid block {
							if(!block.isOpaqueCube()) {
								canHit = false;
							}
							
							// Check if foliage
							if(block.isFoliage(world, bX, bY, bZ)) {
								canHit = false;
							}
							
							// Check if we hit a tile entity
							if(world.getTileEntity(bX, bY, bZ) != null) {
								canHit = false;
							}
							
							// Check again if this is a block we cant break
							for(Block b : this.dontBreakBlocks) {
								if(b == block) {
									canHit = false;
									break;
								}
							}
							
							if(canHit) {
								
								// EXPLOSIONS :D
								if(this.doExplosionDamage) {
									world.newExplosion(this, bX, bY, bZ, explosionSize, false, ConfigSettings.EXPLOSIONS_RESPECT_GAMERULES ? (this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing") ? true : false) : true);
								}
								
								// FIRE :D
								if(this.doFireDamage) {
									setFire(worldObj, xPos, yPos, zPos, side);
								}
							
								else {
									
									float blockHardness = block.getBlockHardness(world, xPos, yPos, zPos);
									
									LogHelper.debug("Block hardness: " + blockHardness);
									
									// Check the hardness of the block
									if(blockHardness <= this.strength) {
										
										if(world.getBlock(xPos, yPos, zPos) != Blocks.bedrock) {
											
											if(this.getThrower() instanceof EntityPlayerMP) {
												
												EntityPlayerMP player = (EntityPlayerMP)this.getThrower();
											
												// Fire a block break event
												BlockEvent.BreakEvent event = ForgeHooks.onBlockBreakEvent(world, player.theItemInWorldManager.getGameType(), player, xPos, yPos, zPos);
												if(!event.isCanceled()) {
													
													if(world.setBlockToAir(xPos, yPos, zPos)) {
														//LogHelper.debug(String.format("Breaking block at: %d, %d, %d", xPos, yPos, zPos));
												
														if(!player.capabilities.isCreativeMode) {
															block.dropBlockAsItem(world, xPos, yPos, zPos, meta, 0);
														}
													}
												}
											}
										}
									}
									
									// If the block is too hard, the laser is killed
									else {
										this.numOfThingsBroken = -1;
										this.setDead();
									}
								}
							}
						}
					}
				}
			}
		}
		
		// Kill this, only on the server
		if(!this.worldObj.isRemote) {
			if(this.numOfThingsBroken < 0) {
				this.setDead();
			}
		}
		
//		else {
//			// Spawn particles and play sounds
//			Random random = new Random();
//			
//			for(int i = 0; i < 4; i++) {
//				float x = (float) (this.posX + random.nextFloat() - 0.5f);
//				float y = (float) (this.posY + random.nextFloat() - 0.5f);
//				float z = (float) (this.posZ + random.nextFloat() - 0.5f);
//				
//				float dX = random.nextFloat();
//				float dY = random.nextFloat();
//				float dZ = random.nextFloat();
//				
//				float scale = 0.25f + random.nextFloat() - 0.25f;
//				int lifeTime = 20;
//				
//				EntityGLParticleBaseFX particle = new EntityGLParticleBaseFX(this.worldObj, x, y, z, dX, dY, dZ, scale, lifeTime, 0, 0.9f, this.getColor());
//				Minecraft.getMinecraft().effectRenderer.addEffect(new EntityGLParticleBaseFX(this.worldObj, x, y, z, dX, dY, dZ, scale, lifeTime, 0, 0.9f, this.getColor()));
//			}
//			//this.worldObj.spawnParticle("snowballpoof", this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
//		}
	}
	
	@Override
	protected float getGravityVelocity() {
		return 0f;
	}
	
	@Override
	public String toString() {
		return "LaserEntity @ " + this.posX + ", " + this.posY + ", " + this.posZ;
	}
	
	@Override
	public void onUpdate() {
		
		this.readEntityFromNBT(getEntityData());
		
		lifeTime++;
		if(lifeTime > maxLifeTime) {
			LogHelper.debug("Destroying laser projectile entity as it is too old");
			this.setDead();
		}
		
		this.writeEntityToNBT(getEntityData());
		
		//System.out.println(this);
		
		super.onUpdate();
	}
	
	public void setDead(boolean destroyInven) {
		
		if(destroyInven) {
			this.attachedItemStack = null;
			this.attachedPower = 0;
		}
		
		setDead();
	}
	
	@Override
	public void setDead() {
		
		
		
		if(this.attachedItemStack != null) {
			EntityItem item = new EntityItem(worldObj, this.posX, this.posY, this.posZ, this.attachedItemStack);
			worldObj.spawnEntityInWorld(item);
		}
		
		super.setDead();
	}
	
	// The hardness of the laser. A higher hardness means it can break harder blocks
	public int getHardness() {
		return this.strength;
	}
	
	public void setHardness(int h) {
		this.strength = (h < 0) ? 0 : h;
	}
	
	// The damage the laser does
	public float getDamage() {
		return this.damage;
	}
	
	public void setDamage(int d) {
		this.damage = (d < 0) ? 0 : d;
	}
	
	// NBT stuff
	public void writeEntityToNBT(NBTTagCompound compound) {
		super.writeEntityToNBT(compound);
		compound.setInteger(lifeTimeTag, this.lifeTime);
	}
	
	public void readEntityFromNBT(NBTTagCompound compound) {
		super.readEntityFromNBT(compound);
		if(compound.hasKey(lifeTimeTag))
			this.lifeTime = compound.getInteger(lifeTimeTag);
	}
	
	// Set fire on the side we hit
	private void setFire(World world, int x, int y, int z, int side) {
		switch(side) {
		case 0:
			y--;
			break;
		case 1:
			y++;
			break;
		case 2:
			z--;
			break;
		case 3:
			z++;
			break;
		case 4:
			x--;
			break;
		case 5:
			x++;
			break;
		}
		
		// Check that this block is air
		if(worldObj.isAirBlock(x, y, z)) {
			world.setBlock(x, y, z, Blocks.fire);
		}
	}
}


