package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Light grey laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserLightGray extends ProjectileLaser {
	
	public ProjectileLaserLightGray(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.LIGHT_GRAY);
	}
	
	public ProjectileLaserLightGray(World world) {
		super(world, Color.LIGHT_GRAY);
	}
}
