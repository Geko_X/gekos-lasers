package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Blue laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserBlue extends ProjectileLaser {
	
	public ProjectileLaserBlue(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.BLUE);
	}
	
	public ProjectileLaserBlue(World world) {
		super(world ,Color.BLUE);
	}
}
