package com.gekox.gekosLasers.utility;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaser;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserBlack;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserBlue;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserBrown;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserCyan;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserGray;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserGreen;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserLightBlue;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserLightGray;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserLime;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserMagenta;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserOrange;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserPink;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserPurple;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserRed;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserWhite;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserYellow;

public class LaserUtils {

	public static ProjectileLaser determineLaser(World world, EntityPlayer player, Color color) {
		
		switch(color) {
		
		case WHITE:
			return new ProjectileLaserWhite(world, player);
		case ORANGE:
			return new ProjectileLaserOrange(world, player);
		case MAGENTA:
			return new ProjectileLaserMagenta(world, player);
		case LIGHT_BLUE:
			return new ProjectileLaserLightBlue(world, player);
		case YELLOW:
			return new ProjectileLaserYellow(world, player);
		case LIME:
			return new ProjectileLaserLime(world, player);
		case PINK:
			return new ProjectileLaserPink(world, player);
		case GRAY:
			return new ProjectileLaserGray(world, player);
		case LIGHT_GRAY:
			return new ProjectileLaserLightGray(world, player);
		case CYAN:
			return new ProjectileLaserCyan(world, player);
		case PURPLE:
			return new ProjectileLaserPurple(world, player);
		case BLUE:
			return new ProjectileLaserBlue(world, player);
		case BROWN:
			return new ProjectileLaserBrown(world, player);
		case GREEN:
			return new ProjectileLaserGreen(world, player);
		case RED:
			return new ProjectileLaserRed(world, player);
		case BLACK:
			return new ProjectileLaserBlack(world, player);
		default:
			return new ProjectileLaserWhite(world, player);
		}
	}
	
	public static ProjectileLaser determineLaser(World world, Color color) {
		
		switch(color) {
		
		case WHITE:
			return new ProjectileLaserWhite(world);
		case ORANGE:
			return new ProjectileLaserOrange(world);
		case MAGENTA:
			return new ProjectileLaserMagenta(world);
		case LIGHT_BLUE:
			return new ProjectileLaserLightBlue(world);
		case YELLOW:
			return new ProjectileLaserYellow(world);
		case LIME:
			return new ProjectileLaserLime(world);
		case PINK:
			return new ProjectileLaserPink(world);
		case GRAY:
			return new ProjectileLaserGray(world);
		case LIGHT_GRAY:
			return new ProjectileLaserLightGray(world);
		case CYAN:
			return new ProjectileLaserCyan(world);
		case PURPLE:
			return new ProjectileLaserPurple(world);
		case BLUE:
			return new ProjectileLaserBlue(world);
		case BROWN:
			return new ProjectileLaserBrown(world);
		case GREEN:
			return new ProjectileLaserGreen(world);
		case RED:
			return new ProjectileLaserRed(world);
		case BLACK:
			return new ProjectileLaserBlack(world);
		default:
			return new ProjectileLaserWhite(world);
		}
	}
	
	public static ProjectileLaser copyLaser(ProjectileLaser laser, ProjectileLaser newLaser) {
		
		newLaser.setupLaser(
				(int) laser.getDamage(),
				laser.strength,
				laser.blockBreakRadius,
				laser.doExplosionDamage,
				laser.explosionSize,
				laser.doFireDamage,
				laser.burnTime,
				laser.doTeleport);
		
		newLaser.attachItemStack(laser.getAttachedItemStack());
		newLaser.attachPower(laser.getAttachedPower());
		newLaser.numOfThingsBroken = laser.numOfThingsBroken;
		
		laser.attachItemStack(null);
		laser.attachPower(0);
		laser.setDead(true);
		
		return newLaser;
		
	}
	
}
