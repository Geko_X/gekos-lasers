package com.gekox.gekosLasers.utility.gui;

import com.gekox.gekosLasers.GekosLasers;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;
import com.gekox.gekosLasers.network.UpdateTurretMessage;
import com.gekox.gekosLasers.utility.LogHelper;

import cofh.lib.gui.GuiBase;
import cofh.lib.gui.element.ElementButtonManaged;
import cofh.lib.gui.element.ElementButtonOption;

public class ToggleButton extends ElementButtonOption {

	private TileEntityTurret turret;
	
	public ToggleButton(GuiBase base, int posX, int posY, int width, int height, TileEntityTurret turret) {
		super(base, posX, posY, width, height);
		this.turret = turret;
	}

	@Override
	public void onValueChanged(int value, String label) {
		
	}

}
