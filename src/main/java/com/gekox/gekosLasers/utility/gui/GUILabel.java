package com.gekox.gekosLasers.utility.gui;

import net.minecraft.client.gui.FontRenderer;
import cofh.lib.gui.GuiBase;
import cofh.lib.gui.element.ElementBase;

public class GUILabel extends ElementBase {

	public String text = "";
	
	public GUILabel(GuiBase base, int x, int y, int w, int h, String text) {
		super(base, x, y, w, h);
		this.text = text;
	}

	@Override
	public void drawBackground(int x, int y, float ticks) {

	}

	@Override
	public void drawForeground(int mouseX, int mouseY) {
		String text = getFontRenderer().trimStringToWidth(this.text, sizeX - 4);
		drawStringNoShadow(getFontRenderer(), text, posX, posY, 4210752);
	}
	
	private void drawCenteredStringNoShadow(FontRenderer fontRenderer, String text, int x, int y, int color) {
		fontRenderer.drawString(" ", x - fontRenderer.getStringWidth(text) / 2, y, color);
		fontRenderer.drawString(text, x - fontRenderer.getStringWidth(text) / 2, y, color);
	}
	
	private void drawStringNoShadow(FontRenderer fontRenderer, String text, int x, int y, int color) {
		fontRenderer.drawString(text, x, y, color);
	}
	
}
