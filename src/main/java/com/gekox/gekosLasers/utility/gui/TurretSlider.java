package com.gekox.gekosLasers.utility.gui;

import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;

import cofh.lib.gui.GuiBase;
import cofh.lib.gui.element.listbox.SliderHorizontal;

public class TurretSlider extends SliderHorizontal {

	public TileEntityTurret turret;
	
	public TurretSlider(GuiBase base, int x, int y, int width, int height, int max, int min, TileEntityTurret turret) {
		super(base, x, y, width, height, max, min);
		this.turret = turret;
	}
	
	public int getValue() {
		return this._value;
	}

}
