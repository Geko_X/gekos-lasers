package com.gekox.gekosLasers.utility;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.EntityAmbientCreature;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;

public class TurretUtil {

	public static float getDistanceToEntity(TileEntityTurret turret, Entity entity) {
		
		float x = (float) (turret.xCoord - entity.posX);
		float y = (float) (turret.yCoord - entity.posY);
		float z = (float) (turret.zCoord - entity.posZ);
		
		return MathHelper.sqrt_float(x * x + y * y + z * z);
		
	}
	
	public static Entity getTarget(TileEntityTurret turret, World world, int x, int y, int z, int range) {
		
		Entity target = null;
		
		if(!world.isRemote && turret != null && turret.getOwner() != null) {
			AxisAlignedBB axis = AxisAlignedBB.getBoundingBox(x - range, y - range, z - range, x + range, y + range, z + range);
			
			List<Entity> targets = world.getEntitiesWithinAABB(Entity.class, axis);
			
			for(Entity e : targets) {
				if(turret.attackMobs) {
					if(!e.isDead && (/*e instanceof EntityAnimal || e instanceof EntityAmbientCreature ||*/ e instanceof IMob)) {
						target = e;
					}
				}
				
				if(turret.attackPlayers) {
					if(!e.isDead && e instanceof EntityPlayerMP) {
						EntityPlayerMP player = (EntityPlayerMP)e;
						
						if(!player.getDisplayName().equals(turret.getOwner()) && !isTrusted(turret, player.getDisplayName())) {
							target = e;
						}
					}
				}
				
				if(target != null && canTurretSeeTarget(turret, (EntityLivingBase) target, range))
					return target;
				
			}
		}
		
		return null;
		
	}
	
	public static boolean canTurretSeeTarget(TileEntityTurret turret, EntityLivingBase target, float range) {
		
		GhostTurretEntity ghost = new GhostTurretEntity(turret.getWorldObj());
		
		ghost.posX = turret.xCoord - 0.5f;
		ghost.posY = turret.yCoord + 0.5f;
		ghost.posZ = turret.zCoord - 0.5f;
		
		if(ghost.getDistanceToEntity(target) > range)
			return false;
		
		return ghost.canEntityBeSeen(target);
		
	}

	private static boolean isTrusted(TileEntityTurret turret, String name) {
		for (String trusted_player : turret.getTrustedPlayers()) {
			if (trusted_player.equals(name)) {
				return true;
			}
		}

		return false;
	}
	
	public static int getRFUsePerShot(float base, float damage, float accuracy, float cooldown) {
		
		damage = damage / 10f;
		accuracy = accuracy / 10f;
		cooldown = cooldown / 10f;
		
		int RFPerShot = (int) (Math.round(base * damage * Math.pow(1 + accuracy, 1 + cooldown)) + 50);
		
		return RFPerShot;
	}
	
	public static int getRFUsePerTick(float base, float range) {
		return Math.round(base + range * range);
	}
	
}

class GhostTurretEntity extends EntityLivingBase {

	public GhostTurretEntity(World world) {
		super(world);
	}

	@Override
	public ItemStack getHeldItem() {
		return null;
	}

	@Override
	public ItemStack getEquipmentInSlot(int p_71124_1_) {
		return null;
	}

	@Override
	public void setCurrentItemOrArmor(int p_70062_1_, ItemStack p_70062_2_) {
		
	}

	@Override
	public ItemStack[] getLastActiveItems() {
		return null;
	}

	
}
