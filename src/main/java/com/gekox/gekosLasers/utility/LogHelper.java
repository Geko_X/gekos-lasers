package com.gekox.gekosLasers.utility;

import org.apache.logging.log4j.Level;

import com.gekox.gekosLasers.reference.Reference;

import cpw.mods.fml.common.FMLLog;

// Pahimar's LogHelper class
// https://www.youtube.com/watch?v=oVq5kslZJc4&list=PLQPiZYWovwmnZlgvbHCbz6TefIgeEiVcj&t=1333

public class LogHelper {

	public static void log(Level logLevel, Object object) {
		FMLLog.log(Reference.MODID, logLevel, String.valueOf(object));
	}
	
	public static void all(Object object) { log(Level.ALL, object); }
	
	public static void debug(Object object) { log(Level.DEBUG, object); }
	
	public static void error(Object object) { log(Level.ERROR, object); }
	
	public static void fatal(Object object) { log(Level.FATAL, object); }
	
	public static void info(Object object) { log(Level.INFO, object); }
	
	public static void off(Object object) { log(Level.OFF, object); }
	
	public static void trace(Object object) { log(Level.TRACE, object); }
	
	public static void warm(Object object) { log(Level.WARN, object); }
	
}
