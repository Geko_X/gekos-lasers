package com.gekox.gekosLasers.client.renderer.item;

import static net.minecraftforge.client.IItemRenderer.ItemRenderType.EQUIPPED_FIRST_PERSON;
import static net.minecraftforge.client.IItemRenderer.ItemRenderType.FIRST_PERSON_MAP;

import java.nio.FloatBuffer;
import java.util.Random;
import java.lang.reflect.Field;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import com.gekox.gekosLasers.items.laserSwords.ItemTier1LaserSword;
import com.gekox.gekosLasers.items.laserSwords.ItemTier2LaserSword;
import com.gekox.gekosLasers.items.laserSwords.ItemTier3LaserSword;
import com.gekox.gekosLasers.items.laserSwords.ItemTier4LaserSword;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.Reference;
import com.gekox.gekosLasers.utility.Color;
import com.gekox.gekosLasers.utility.NBTHelper;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemCloth;
import net.minecraft.item.ItemMap;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Timer;
import net.minecraft.world.storage.MapData;
import net.minecraftforge.client.ForgeHooksClient;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;

public class RenderLaserSword extends ItemRenderer implements IItemRenderer  {

	private static RenderItem renderItem = new RenderItem();
	private static final ResourceLocation beamLocation = new ResourceLocation(Reference.RESOURCE_PREFIX + LaserReference.SWORD_BEAM_TEXTURE);
	private static final ResourceLocation swordLocation1 = new ResourceLocation(Reference.RESOURCE_PREFIX + LaserReference.TIER_1_SWORD_HILT_TEXTURE);
	private static final ResourceLocation swordLocation2 = new ResourceLocation(Reference.RESOURCE_PREFIX + LaserReference.TIER_2_SWORD_HILT_TEXTURE);
	private static final ResourceLocation swordLocation3 = new ResourceLocation(Reference.RESOURCE_PREFIX + LaserReference.TIER_3_SWORD_HILT_TEXTURE);
	private static final ResourceLocation swordLocation4 = new ResourceLocation(Reference.RESOURCE_PREFIX + LaserReference.TIER_4_SWORD_HILT_TEXTURE);
	
	private float normal = 0.7f;
	
	private Minecraft mc = Minecraft.getMinecraft();
	private float prevEquippedProgress;
	private float equippedProgress;
	private ItemStack itemToRender;
	private int equippedItemSlot = -1;
	
	private float currentTick = 0;
	private float prevTick = -1;
	
	public RenderLaserSword(Minecraft mc) {
		super(mc);
	}
	
	public RenderLaserSword() {
		this(Minecraft.getMinecraft());
	}

	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return (type == ItemRenderType.EQUIPPED); //return (type == ItemRenderType.INVENTORY) || // || (type == ItemRenderType.ENTITY);
	}
	
	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item,
			ItemRendererHelper helper) {
		return false;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack stack, Object... data) {
		
		GL11.glPushMatrix();
		
		switch(type) {
		
		case INVENTORY:
			renderInventory(stack, data);
			break;
			
		case EQUIPPED_FIRST_PERSON:
			try {
				renderFirstPerson(stack, data);
			} catch (NoSuchFieldException e) {
				System.out.println("No such field");
				e.printStackTrace();
			} catch (SecurityException e) {
				System.out.println("Security");
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				System.out.println("Illegal argument");
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				System.out.println("Illegal access");
				e.printStackTrace();
			}
			break;
			
		case EQUIPPED:
			renderEquipped(stack, data);
			break;
			
		case ENTITY:
			renderAsEntity(stack, data);
			break;
		
		default:
			break;
		
		}
		
		GL11.glPopMatrix();
	}
	
	private void renderAsEntity(ItemStack stack, Object... data) {
		
		EntityClientPlayerMP player = Minecraft.getMinecraft().thePlayer;
		
		Tessellator tessellator = Tessellator.instance;
		FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
		TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();
		RenderManager renderManager = RenderManager.instance;
		Item item = stack.getItem();
		Block block = Block.getBlockFromItem(item);
		
		int colorIndex = NBTHelper.getInt(stack, LaserReference.LASER_COLOR_TAG);
		Color color = Color.color[colorIndex];
		
		//boolean isActive = NBTHelper.getBoolean(stack, LaserReference.SWORD_ACTIVE_TAG);
		boolean isActive = true;
		Random random = new Random();
		
		double time = (double)(Minecraft.getSystemTime() % 3000L) / 3000.0F * 4.0F;
		float offset = (float)MathHelper.sin((float) time) * 0.225f;
		
		float f14 = 0;
		float f15 = 1;
		float f4 = 0;
		float f5 = 1;
		float f6 = 1.0F;
		float f7 = 0.5F;
		float f8 = 0.25F;
		float f10;
		
		// Render item in 3D. For now, this is disabled
//		//if (renderManager.options.fancyGraphics) {
//			GL11.glPushMatrix();
//
//			//if (renderInFrame)
//			//{
//			GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
//			//}
//			//else {
//				//GL11.glRotatef((((float)p_77020_1_.age + p_77020_4_) / 20.0F + p_77020_1_.hoverStart) * (180F / (float)Math.PI), 0.0F, 1.0F, 0.0F);
//			//}
//
//			float f9 = 0.0625F;
//			f10 = 0.021875F;
//			int j = stack.stackSize;
//			byte b0;
//
//			if (j < 2) {
//				b0 = 1;
//			}
//			else if (j < 16) {
//				b0 = 2;
//			}
//			else if (j < 32) {
//				b0 = 3;
//			}
//			else {
//				b0 = 4;
//			}
//
//			b0 = 1;
//
//			GL11.glTranslatef(-f7, -f8, -((f9 + f10) * (float)b0 / 2.0F));
//			
//			for (int k = 0; k < b0; ++k) {
//				// Makes items offset when in 3D, like when in 2D, looks much better. Considered a vanilla bug...
//				if (k > 0) {
//					float x = (random.nextFloat() * 2.0F - 1.0F) * 0.3F / 0.5F;
//					float y = (random.nextFloat() * 2.0F - 1.0F) * 0.3F / 0.5F;
//					float z = (random.nextFloat() * 2.0F - 1.0F) * 0.3F / 0.5F;
//					GL11.glTranslatef(x, y, f9 + f10);
//				}
//				else {
//					GL11.glTranslatef(0f, 0f, f9 + f10);
//				}
//
//				texturemanager.bindTexture(swordLocation);
//				ItemRenderer.renderItemIn2D(tessellator, f15, f4, f14, f5, 16, 16, f9);
//				
//				if(isActive) {
//					texturemanager.bindTexture(beamLocation);
//					tessellator.setColorRGBA(color.r, color.g, color.b, color.a);
//					ItemRenderer.renderItemIn2D(tessellator, f15, f4, f14, f5, 16, 16, f9);
//				}
//			}
//			
//			GL11.glPopMatrix();
//		}
		
		//else {
			
			for (int l = 0; l < 1; ++l) {
				GL11.glPushMatrix();

				if (l > 0) {
					f10 = (random.nextFloat() * 2.0F - 1.0F) * 0.3F;
					float f16 = (random.nextFloat() * 2.0F - 1.0F) * 0.3F;
					float f17 = (random.nextFloat() * 2.0F - 1.0F) * 0.3F;
					GL11.glTranslatef(f10, f16, f17);
				}

				GL11.glTranslatef(0, 0.2f + offset, 0);
				
				//if (!renderInFrame)
				//{
				GL11.glRotatef(180.0F - renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
				//}
				
				if(item instanceof ItemTier1LaserSword) {
					texturemanager.bindTexture(swordLocation1);
				}
				
				if(item instanceof ItemTier2LaserSword) {
					texturemanager.bindTexture(swordLocation2);
				}
				
				if(item instanceof ItemTier3LaserSword) {
					texturemanager.bindTexture(swordLocation3);
				}
				
				if(item instanceof ItemTier4LaserSword) {
					texturemanager.bindTexture(swordLocation4);
				}
				
				tessellator.startDrawingQuads();
				tessellator.setNormal(0.0F, 1.0F, 0.0F);
				tessellator.addVertexWithUV((double)(0.0F - f7), (double)(0.0F - f8), 0.0D, (double)f14, (double)f5);
				tessellator.addVertexWithUV((double)(f6 - f7), (double)(0.0F - f8), 0.0D, (double)f15, (double)f5);
				tessellator.addVertexWithUV((double)(f6 - f7), (double)(1.0F - f8), 0.0D, (double)f15, (double)f4);
				tessellator.addVertexWithUV((double)(0.0F - f7), (double)(1.0F - f8), 0.0D, (double)f14, (double)f4);
				tessellator.draw();
				
				if(isActive) {
					texturemanager.bindTexture(beamLocation);
					tessellator.startDrawingQuads();
					tessellator.setColorRGBA(color.r, color.g, color.b, color.a);
					tessellator.setNormal(0.0F, normal, 0.0F);
					tessellator.addVertexWithUV((double)(0.0F - f7), (double)(0.0F - f8), 0.0D, (double)f14, (double)f5);
					tessellator.addVertexWithUV((double)(f6 - f7), (double)(0.0F - f8), 0.0D, (double)f15, (double)f5);
					tessellator.addVertexWithUV((double)(f6 - f7), (double)(1.0F - f8), 0.0D, (double)f15, (double)f4);
					tessellator.addVertexWithUV((double)(0.0F - f7), (double)(1.0F - f8), 0.0D, (double)f14, (double)f4);
					tessellator.draw();
				}
				
				GL11.glPopMatrix();
			}
		//}
		
	}

	private void renderInventory(ItemStack stack, Object... data) {
		
		Tessellator tessellator = Tessellator.instance;
		FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
		TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();
		Item item = stack.getItem();
		Block block = Block.getBlockFromItem(item);
		
		int colorIndex = NBTHelper.getInt(stack, LaserReference.LASER_COLOR_TAG);
		Color color = Color.color[colorIndex];
		//boolean isActive = NBTHelper.getBoolean(stack, LaserReference.SWORD_ACTIVE_TAG);
		boolean isActive = true;
		
		if(item instanceof ItemTier1LaserSword) {
			texturemanager.bindTexture(swordLocation1);
		}
		
		if(item instanceof ItemTier2LaserSword) {
			texturemanager.bindTexture(swordLocation2);
		}
		
		if(item instanceof ItemTier3LaserSword) {
			texturemanager.bindTexture(swordLocation3);
		}
		
		if(item instanceof ItemTier4LaserSword) {
			texturemanager.bindTexture(swordLocation4);
		}
		
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV(0, 16, 0, 0, 1);
		tessellator.addVertexWithUV(16, 16, 0, 1, 1);
		tessellator.addVertexWithUV(16, 0, 0, 1, 0);
		tessellator.addVertexWithUV(0, 0, 0, 0, 0);
		tessellator.draw();
		
		if(isActive) {
		
			texturemanager.bindTexture(beamLocation);
			tessellator.startDrawingQuads();
			tessellator.setColorRGBA(color.r, color.g, color.b, color.a);
			tessellator.addVertexWithUV(0, 16, 0, 0, 1);
			tessellator.addVertexWithUV(16, 16, 0, 1, 1);
			tessellator.addVertexWithUV(16, 0, 0, 1, 0);
			tessellator.addVertexWithUV(0, 0, 0, 0, 0);
			tessellator.draw();
		}
		
	}
	
	private void renderFirstPerson(ItemStack stack, Object... data) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

		Field timerField = Minecraft.class.getDeclaredField("timer");
		timerField.setAccessible(true);
		Timer timer = (Timer) timerField.get(this.mc);
		float partialTickTime = timer.elapsedPartialTicks;
		
		Tessellator tessellator = Tessellator.instance;
		FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
		TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();
		Item item = stack.getItem();
		Block block = Block.getBlockFromItem(item);
		
		float f1 = this.prevEquippedProgress + (this.equippedProgress - this.prevEquippedProgress) * partialTickTime;
		EntityClientPlayerMP entityclientplayermp = this.mc.thePlayer;
		float f2 = entityclientplayermp.prevRotationPitch + (entityclientplayermp.rotationPitch - entityclientplayermp.prevRotationPitch) * partialTickTime;
		GL11.glPushMatrix();
		GL11.glRotatef(f2, 1.0F, 0.0F, 0.0F);
		GL11.glRotatef(entityclientplayermp.prevRotationYaw + (entityclientplayermp.rotationYaw - entityclientplayermp.prevRotationYaw) * partialTickTime, 0.0F, 1.0F, 0.0F);
		RenderHelper.enableStandardItemLighting();
		GL11.glPopMatrix();
		EntityPlayerSP entityplayersp = (EntityPlayerSP)entityclientplayermp;
		float f3 = entityplayersp.prevRenderArmPitch + (entityplayersp.renderArmPitch - entityplayersp.prevRenderArmPitch) * partialTickTime;
		float f4 = entityplayersp.prevRenderArmYaw + (entityplayersp.renderArmYaw - entityplayersp.prevRenderArmYaw) * partialTickTime;
		GL11.glRotatef((entityclientplayermp.rotationPitch - f3) * 0.1F, 1.0F, 0.0F, 0.0F);
		GL11.glRotatef((entityclientplayermp.rotationYaw - f4) * 0.1F, 0.0F, 1.0F, 0.0F);
		
		this.updateEquippedItem();
		ItemStack itemstack = this.itemToRender;

		int i = this.mc.theWorld.getLightBrightnessForSkyBlocks(MathHelper.floor_double(entityclientplayermp.posX), MathHelper.floor_double(entityclientplayermp.posY), MathHelper.floor_double(entityclientplayermp.posZ), 0);
		int j = i % 65536;
		int k = i / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float)j / 1.0F, (float)k / 1.0F);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		float f5;
		float f6;
		float f7;

		if (itemstack != null) {
			int l = itemstack.getItem().getColorFromItemStack(itemstack, 0);
			f5 = (float)(l >> 16 & 255) / 255.0F;
			f6 = (float)(l >> 8 & 255) / 255.0F;
			f7 = (float)(l & 255) / 255.0F;
			GL11.glColor4f(f5, f6, f7, 1.0F);
		}
		
		else{
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		}

		float f8;
		float f9;
		float f10;
		float f13;
		Render render;
		RenderPlayer renderplayer;

		if (itemstack != null) {
			GL11.glPushMatrix();
			f13 = 0.8F;

			if (entityclientplayermp.getItemInUseCount() > 0) {
				EnumAction enumaction = itemstack.getItemUseAction();
			}
			else {
				f5 = entityclientplayermp.getSwingProgress(partialTickTime);
				f6 = MathHelper.sin(f5 * (float)Math.PI);
				f7 = MathHelper.sin(MathHelper.sqrt_float(f5) * (float)Math.PI);
				GL11.glTranslatef(-f7 * 0.4F, MathHelper.sin(MathHelper.sqrt_float(f5) * (float)Math.PI * 2.0F) * 0.2F, -f6 * 0.2F);
			}
			
			GL11.glTranslatef(0.7F * f13, -0.65F * f13 - (1.0F - f1) * 0.6F, -0.9F * f13);
			GL11.glRotatef(45.0F, 0.0F, 1.0F, 0.0F);
			GL11.glEnable(GL12.GL_RESCALE_NORMAL);
			f5 = entityclientplayermp.getSwingProgress(partialTickTime);
			f6 = MathHelper.sin(f5 * f5 * (float)Math.PI);
			f7 = MathHelper.sin(MathHelper.sqrt_float(f5) * (float)Math.PI);
			GL11.glRotatef(-f6 * 20.0F, 0.0F, 1.0F, 0.0F);
			GL11.glRotatef(-f7 * 20.0F, 0.0F, 0.0F, 1.0F);
			GL11.glRotatef(-f7 * 80.0F, 1.0F, 0.0F, 0.0F);
			f8 = 0.4F;
			GL11.glScalef(f8, f8, f8);
			float f11;
			float f12;

			if (entityclientplayermp.getItemInUseCount() > 0) {
				
				EnumAction enumaction1 = itemstack.getItemUseAction();

				if (enumaction1 == EnumAction.block) {
					GL11.glTranslatef(-0.5F, 0.2F, 0.0F);
					GL11.glRotatef(30.0F, 0.0F, 1.0F, 0.0F);
					GL11.glRotatef(-80.0F, 1.0F, 0.0F, 0.0F);
					GL11.glRotatef(60.0F, 0.0F, 1.0F, 0.0F);
				}
			}

			if (itemstack.getItem().shouldRotateAroundWhenRendering()) {
				GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
			}
			
			if (itemstack.getItem().requiresMultipleRenderPasses()) {
				this.renderItem(entityclientplayermp, itemstack, 0, EQUIPPED_FIRST_PERSON);
				for (int x = 1; x < itemstack.getItem().getRenderPasses(itemstack.getItemDamage()); x++)
				{
					int k1 = itemstack.getItem().getColorFromItemStack(itemstack, x);
					f10 = (float)(k1 >> 16 & 255) / 255.0F;
					f11 = (float)(k1 >> 8 & 255) / 255.0F;
					f12 = (float)(k1 & 255) / 255.0F;
					GL11.glColor4f(1.0F * f10, 1.0F * f11, 1.0F * f12, 1.0F);
					renderItem(entityclientplayermp, itemstack, x, EQUIPPED_FIRST_PERSON);
				}
			}
			else {
				renderItem(entityclientplayermp, itemstack, 0, EQUIPPED_FIRST_PERSON);
			}
			GL11.glPopMatrix();
		}
		
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		RenderHelper.disableStandardItemLighting();
	}
	
	private void renderEquipped(ItemStack stack, Object... data) {
		
		Tessellator tessellator = Tessellator.instance;
		TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();
		RenderManager renderManager = RenderManager.instance;
		Item item = stack.getItem();
		Block block = Block.getBlockFromItem(item);
		
		Entity entity = (Entity) data[1];;
		
		int colorIndex = NBTHelper.getInt(stack, LaserReference.LASER_COLOR_TAG);
		Color color = Color.color[colorIndex];
		
		//boolean isActive = NBTHelper.getBoolean(stack, LaserReference.SWORD_ACTIVE_TAG);
		boolean isActive = true;
		
		GL11.glScalef(1.2f, 1.2f, 1.2f);
		
		// Transformations to make it look like something "realistic"
		GL11.glTranslatef(0.45f, 0, 0);
		GL11.glRotatef(90, 0, 1, 0);
		GL11.glRotatef(45, 0, 0, 1);
		
		// Blocking rotation
		if((entity instanceof EntityPlayer) && ((EntityPlayer)entity).isBlocking()) {
			GL11.glRotatef(-60, 0, 0, 1);
			GL11.glRotatef(-75, 0, 1, 0);
		}
		
		// Draw the hilt
		if(item instanceof ItemTier1LaserSword) {
			texturemanager.bindTexture(swordLocation1);
		}
		
		if(item instanceof ItemTier2LaserSword) {
			texturemanager.bindTexture(swordLocation2);
		}
		
		if(item instanceof ItemTier3LaserSword) {
			texturemanager.bindTexture(swordLocation3);
		}
		
		if(item instanceof ItemTier4LaserSword) {
			texturemanager.bindTexture(swordLocation4);
		}
		
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV(0.0D, 0.0D, 0.0D, (double)0, (double)1);
		tessellator.addVertexWithUV(1.0D, 0.0D, 0.0D, (double)1, (double)1);
		tessellator.addVertexWithUV(1.0D, 1.0D, 0.0D, (double)1, (double)0);
		tessellator.addVertexWithUV(0.0D, 1.0D, 0.0D, (double)0, (double)0);
		tessellator.draw();
		
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 0.0F, -1.0F);
		tessellator.addVertexWithUV(0.0D, 1.0D, (double)(0.0F - 0.0625f), (double)0, (double)0);
		tessellator.addVertexWithUV(1.0D, 1.0D, (double)(0.0F - 0.0625f), (double)1, (double)0);
		tessellator.addVertexWithUV(1.0D, 0.0D, (double)(0.0F - 0.0625f), (double)1, (double)1);
		tessellator.addVertexWithUV(0.0D, 0.0D, (double)(0.0F - 0.0625f), (double)0, (double)1);
		tessellator.draw();
		
		float f5 = 0.5F * (0 - 1) / (float)16;
		float f6 = 0.5F * (1 - 0) / (float)16;
		tessellator.startDrawingQuads();
		tessellator.setNormal(-1.0F, 0.0F, 0.0F);
		int k;
		float f7;
		float f8;
		for (k = 0; k < 16; ++k) {
			f7 = (float)k / (float)16;
			f8 = 0 + (1 - 0) * f7 - f5;
			tessellator.addVertexWithUV((double)f7, 0.0D, (double)(0.0F - 0.0625f), (double)f8, (double)1);
			tessellator.addVertexWithUV((double)f7, 0.0D, 0.0D, (double)f8, (double)1);
			tessellator.addVertexWithUV((double)f7, 1.0D, 0.0D, (double)f8, (double)0);
			tessellator.addVertexWithUV((double)f7, 1.0D, (double)(0.0F - 0.0625f), (double)f8, (double)0);
		}

		tessellator.draw();
		
		tessellator.startDrawingQuads();
		tessellator.setNormal(1.0F, 0.0F, 0.0F);
		float f9;
		for (k = 0; k < 16; ++k) {
			f7 = (float)k / (float)16;
			f8 = 0 + (1 - 0) * f7 - f5;
			f9 = f7 + 1.0F / (float)16;
			tessellator.addVertexWithUV((double)f9, 1.0D, (double)(0.0F - 0.0625f), (double)f8, (double)0);
			tessellator.addVertexWithUV((double)f9, 1.0D, 0.0D, (double)f8, (double)0);
			tessellator.addVertexWithUV((double)f9, 0.0D, 0.0D, (double)f8, (double)1);
			tessellator.addVertexWithUV((double)f9, 0.0D, (double)(0.0F - 0.0625f), (double)f8, (double)1);
		}

		tessellator.draw();
		
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 1.0F, 0.0F);
		for (k = 0; k < 16; ++k) {
			f7 = (float)k / (float)16;
			f8 = 1 + (0 - 1) * f7 - f6;
			f9 = f7 + 1.0F / (float)16;
			tessellator.addVertexWithUV(0.0D, (double)f9, 0.0D, (double)0, (double)f8);
			tessellator.addVertexWithUV(1.0D, (double)f9, 0.0D, (double)1, (double)f8);
			tessellator.addVertexWithUV(1.0D, (double)f9, (double)(0.0F - 0.0625f), (double)1, (double)f8);
			tessellator.addVertexWithUV(0.0D, (double)f9, (double)(0.0F - 0.0625f), (double)0, (double)f8);
		}

		tessellator.draw();
		
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, -1.0F, 0.0F);
		for (k = 0; k < 16; ++k) {
			f7 = (float)k / (float)16;
			f8 = 1 + (0 - 1) * f7 - f6;
			tessellator.addVertexWithUV(1.0D, (double)f7, 0.0D, (double)1, (double)f8);
			tessellator.addVertexWithUV(0.0D, (double)f7, 0.0D, (double)0, (double)f8);
			tessellator.addVertexWithUV(0.0D, (double)f7, (double)(0.0F - 0.0625f), (double)0, (double)f8);
			tessellator.addVertexWithUV(1.0D, (double)f7, (double)(0.0F - 0.0625f), (double)1, (double)f8);
		}

	  tessellator.draw();
	  
	  if(isActive) {
			
		  // Draw the beam, if active
		  
		  texturemanager.bindTexture(beamLocation);
			
			tessellator.startDrawingQuads();
			tessellator.setColorRGBA(color.r, color.g, color.b, color.a);
			tessellator.addVertexWithUV(0.0D, 0.0D, 0.0D, (double)0, (double)1);
			tessellator.addVertexWithUV(1.0D, 0.0D, 0.0D, (double)1, (double)1);
			tessellator.addVertexWithUV(1.0D, 1.0D, 0.0D, (double)1, (double)0);
			tessellator.addVertexWithUV(0.0D, 1.0D, 0.0D, (double)0, (double)0);
			tessellator.draw();
			
			tessellator.startDrawingQuads();
			tessellator.setColorRGBA(color.r, color.g, color.b, color.a);
			tessellator.setNormal(0.0F, 0.0F, -normal);
			tessellator.addVertexWithUV(0.0D, 1.0D, (double)(0.0F - 0.0625f), (double)0, (double)0);
			tessellator.addVertexWithUV(1.0D, 1.0D, (double)(0.0F - 0.0625f), (double)1, (double)0);
			tessellator.addVertexWithUV(1.0D, 0.0D, (double)(0.0F - 0.0625f), (double)1, (double)1);
			tessellator.addVertexWithUV(0.0D, 0.0D, (double)(0.0F - 0.0625f), (double)0, (double)1);
			tessellator.draw();
			
			f5 = 0.5F * (0 - 1) / (float)16;
			f6 = 0.5F * (1 - 0) / (float)16;
			tessellator.startDrawingQuads();
			tessellator.setColorRGBA(color.r, color.g, color.b, color.a);
			tessellator.setNormal(-normal, 0.0F, 0.0F);
			for (k = 0; k < 16; ++k) {
				f7 = (float)k / (float)16;
				f8 = 0 + (1 - 0) * f7 - f5;
				tessellator.addVertexWithUV((double)f7, 0.0D, (double)(0.0F - 0.0625f), (double)f8, (double)1);
				tessellator.addVertexWithUV((double)f7, 0.0D, 0.0D, (double)f8, (double)1);
				tessellator.addVertexWithUV((double)f7, 1.0D, 0.0D, (double)f8, (double)0);
				tessellator.addVertexWithUV((double)f7, 1.0D, (double)(0.0F - 0.0625f), (double)f8, (double)0);
			}

			tessellator.draw();
			
			tessellator.startDrawingQuads();
			tessellator.setColorRGBA(color.r, color.g, color.b, color.a);
			tessellator.setNormal(normal, 0.0F, 0.0F);
			for (k = 0; k < 16; ++k) {
				f7 = (float)k / (float)16;
				f8 = 0 + (1 - 0) * f7 - f5;
				f9 = f7 + 1.0F / (float)16;
				tessellator.addVertexWithUV((double)f9, 1.0D, (double)(0.0F - 0.0625f), (double)f8, (double)0);
				tessellator.addVertexWithUV((double)f9, 1.0D, 0.0D, (double)f8, (double)0);
				tessellator.addVertexWithUV((double)f9, 0.0D, 0.0D, (double)f8, (double)1);
				tessellator.addVertexWithUV((double)f9, 0.0D, (double)(0.0F - 0.0625f), (double)f8, (double)1);
			}

			tessellator.draw();
			
			tessellator.startDrawingQuads();
			tessellator.setColorRGBA(color.r, color.g, color.b, color.a);
			tessellator.setNormal(0.0F, normal, 0.0F);
			for (k = 0; k < 16; ++k) {
				f7 = (float)k / (float)16;
				f8 = 1 + (0 - 1) * f7 - f6;
				f9 = f7 + 1.0F / (float)16;
				tessellator.addVertexWithUV(0.0D, (double)f9, 0.0D, (double)0, (double)f8);
				tessellator.addVertexWithUV(1.0D, (double)f9, 0.0D, (double)1, (double)f8);
				tessellator.addVertexWithUV(1.0D, (double)f9, (double)(0.0F - 0.0625f), (double)1, (double)f8);
				tessellator.addVertexWithUV(0.0D, (double)f9, (double)(0.0F - 0.0625f), (double)0, (double)f8);
			}

			tessellator.draw();
			
			tessellator.startDrawingQuads();
			tessellator.setColorRGBA(color.r, color.g, color.b, color.a);
			tessellator.setNormal(0.0F, normal, 0.0F);
			for (k = 0; k < 16; ++k) {
				f7 = (float)k / (float)16;
				f8 = 1 + (0 - 1) * f7 - f6;
				tessellator.addVertexWithUV(1.0D, (double)f7, 0.0D, (double)1, (double)f8);
				tessellator.addVertexWithUV(0.0D, (double)f7, 0.0D, (double)0, (double)f8);
				tessellator.addVertexWithUV(0.0D, (double)f7, (double)(0.0F - 0.0625f), (double)0, (double)f8);
				tessellator.addVertexWithUV(1.0D, (double)f7, (double)(0.0F - 0.0625f), (double)1, (double)f8);
			}

			tessellator.draw();
		}
	}
	
	@Override
	public void updateEquippedItem() {
		
		this.prevEquippedProgress = this.equippedProgress;
		EntityClientPlayerMP entityclientplayermp = this.mc.thePlayer;
		ItemStack itemstack = entityclientplayermp.inventory.getCurrentItem();
		boolean flag = this.equippedItemSlot == entityclientplayermp.inventory.currentItem && itemstack == this.itemToRender;

		if (this.itemToRender == null && itemstack == null) {
			flag = true;
		}

		if (itemstack != null && this.itemToRender != null && itemstack != this.itemToRender && itemstack.getItem() == this.itemToRender.getItem() && itemstack.getItemDamage() == this.itemToRender.getItemDamage()) {
			this.itemToRender = itemstack;
			flag = true;
		}

		float f = 0.4F;
		float f1 = flag ? 1.0F : 0.0F;
		float f2 = f1 - this.equippedProgress;

		if (f2 < -f) {
			f2 = -f;
		}

		if (f2 > f) {
			f2 = f;
		}

		this.equippedProgress += f2;

		if (this.equippedProgress < 0.1F) {
			this.itemToRender = itemstack;
			this.equippedItemSlot = entityclientplayermp.inventory.currentItem;
		}
	}

	/**
	 * Resets equippedProgress
	 */
	@Override
	public void resetEquippedProgress() {
		this.equippedProgress = 0.0F;
	}

	/**
	 * Resets equippedProgress
	 */
	@Override
	public void resetEquippedProgress2() {
		this.equippedProgress = 0.0F;
	}
	
	@Override
	public void renderItem(EntityLivingBase entLivingBase, ItemStack stack, int meta, ItemRenderType type) {
		
//		System.out.println("Render item");
		
		GL11.glPushMatrix();
		
		Tessellator tessellator = Tessellator.instance;
		TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();
		RenderManager renderManager = RenderManager.instance;
		Item item = stack.getItem();
		
		int colorIndex = NBTHelper.getInt(stack, LaserReference.LASER_COLOR_TAG);
		Color color = Color.color[colorIndex];
		
		//boolean isActive = NBTHelper.getBoolean(stack, LaserReference.SWORD_ACTIVE_TAG);
		boolean isActive = true;
		
		Random random = new Random();

		if(item instanceof ItemTier1LaserSword) {
			texturemanager.bindTexture(swordLocation1);
		}
		
		if(item instanceof ItemTier2LaserSword) {
			texturemanager.bindTexture(swordLocation2);
		}
		
		if(item instanceof ItemTier3LaserSword) {
			texturemanager.bindTexture(swordLocation3);
		}
		
		if(item instanceof ItemTier4LaserSword) {
			texturemanager.bindTexture(swordLocation4);
		}
		
		TextureUtil.func_152777_a(false, false, 1.0F);
		
		float f = 1;
		float f1 = 0;
		float f2 = 0;
		float f3 = 1;
		float f4 = 0.0F;
		float f5 = 0.3F;
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		GL11.glTranslatef(-f4, -f5, 0.0F);
		float f6 = 1.5F;
		GL11.glScalef(f6, f6, f6);
		GL11.glRotatef(50.0F, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(335.0F, 0.0F, 0.0F, 1.0F);
		GL11.glTranslatef(-0.9375F, -0.0625F, 0.0F);
		
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 0.0F, 1.0F);
		tessellator.addVertexWithUV(0.0D, 0.0D, 0.0D, (double)f1, (double)f3);
		tessellator.addVertexWithUV(1.0D, 0.0D, 0.0D, (double)f, (double)f3);
		tessellator.addVertexWithUV(1.0D, 1.0D, 0.0D, (double)f, (double)f2);
		tessellator.addVertexWithUV(0.0D, 1.0D, 0.0D, (double)f1, (double)f2);
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 0.0F, -1.0F);
		tessellator.addVertexWithUV(0.0D, 1.0D, (double)(0.0F - 0.0265f), (double)f1, (double)f2);
		tessellator.addVertexWithUV(1.0D, 1.0D, (double)(0.0F - 0.0265f), (double)f, (double)f2);
		tessellator.addVertexWithUV(1.0D, 0.0D, (double)(0.0F - 0.0265f), (double)f, (double)f3);
		tessellator.addVertexWithUV(0.0D, 0.0D, (double)(0.0F - 0.0265f), (double)f1, (double)f3);
		tessellator.draw();
		float f25 = 0.5F * (f1 - f) / (float)16;
		float f26 = 0.5F * (f3 - f2) / (float)16;
		tessellator.startDrawingQuads();
		tessellator.setNormal(-1.0F, 0.0F, 0.0F);
		int k;
		float f7;
		float f8;

		for (k = 0; k < 16; ++k)
		{
			f7 = (float)k / (float)16;
			f8 = f1 + (f - f1) * f7 - f25;
			tessellator.addVertexWithUV((double)f7, 0.0D, (double)(0.0F - 0.0265f), (double)f8, (double)f3);
			tessellator.addVertexWithUV((double)f7, 0.0D, 0.0D, (double)f8, (double)f3);
			tessellator.addVertexWithUV((double)f7, 1.0D, 0.0D, (double)f8, (double)f2);
			tessellator.addVertexWithUV((double)f7, 1.0D, (double)(0.0F - 0.0265f), (double)f8, (double)f2);
		}

		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(1.0F, 0.0F, 0.0F);
		float f9;

		for (k = 0; k < 16; ++k)
		{
			f7 = (float)k / (float)16;
			f8 = f1 + (f - f1) * f7 - f25;
			f9 = f7 + 1.0F / (float)16;
			tessellator.addVertexWithUV((double)f9, 1.0D, (double)(0.0F - 0.0265f), (double)f8, (double)f2);
			tessellator.addVertexWithUV((double)f9, 1.0D, 0.0D, (double)f8, (double)f2);
			tessellator.addVertexWithUV((double)f9, 0.0D, 0.0D, (double)f8, (double)1);
			tessellator.addVertexWithUV((double)f9, 0.0D, (double)(0.0F - 0.0265f), (double)f8, (double)f3);
		}

		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 1.0F, 0.0F);

		for (k = 0; k < 16; ++k)
		{
			f7 = (float)k / (float)16;
			f8 = 16 + (f2 - 16) * f7 - f26;
			f9 = f7 + 1.0F / (float)16;
			tessellator.addVertexWithUV(0.0D, (double)f9, 0.0D, (double)f1, (double)f8);
			tessellator.addVertexWithUV(1.0D, (double)f9, 0.0D, (double)f, (double)f8);
			tessellator.addVertexWithUV(1.0D, (double)f9, (double)(0.0F - 0.0265f), (double)f, (double)f8);
			tessellator.addVertexWithUV(0.0D, (double)f9, (double)(0.0F - 0.0265f), (double)f1, (double)f8);
		}

		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, -1.0F, 0.0F);

		for (k = 0; k < 16; ++k)
		{
			f7 = (float)k / (float)16;
			f8 = 16 + (f2 - 16) * f7 - f26;
			tessellator.addVertexWithUV(1.0D, (double)f7, 0.0D, (double)f, (double)f8);
			tessellator.addVertexWithUV(0.0D, (double)f7, 0.0D, (double)f1, (double)f8);
			tessellator.addVertexWithUV(0.0D, (double)f7, (double)(0.0F - 0.0265f), (double)f1, (double)f8);
			tessellator.addVertexWithUV(1.0D, (double)f7, (double)(0.0F - 0.0265f), (double)f, (double)f8);
		}

		tessellator.draw();
		
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		//texturemanager.bindTexture(this.beamLocation);
		//TextureUtil.func_147945_b();

		GL11.glPopMatrix();
	}
		
		
//		GL11.glPushMatrix();
//		
//		Tessellator tessellator = Tessellator.instance;
//		TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();
//		RenderManager renderManager = RenderManager.instance;
//		Item item = stack.getItem();
//		Block block = Block.getBlockFromItem(item);
//		
//		int colorIndex = NBTHelper.getInt(stack, LaserReference.LASER_COLOR_TAG);
//		Color color = Color.color[colorIndex];
//		
//		Entity entity = (Entity)entLivingBase;
//		
//		boolean isActive = NBTHelper.getBoolean(stack, LaserReference.SWORD_ACTIVE_TAG);
//		
//		Random random = new Random();
//		
//		// Transformations to make it look like something "realistic"
//		GL11.glTranslatef(0.45f, 0, 0);
//		GL11.glRotatef(90, 0, 1, 0);
//		GL11.glRotatef(45, 0, 0, 1);
//		
//		// Blocking rotation
//		if((entity instanceof EntityPlayer) && ((EntityPlayer)entity).isBlocking()) {
//			GL11.glRotatef(-60, 0, 0, 1);
//			GL11.glRotatef(-75, 0, 1, 0);
//		}
//				
//		// Draw the hilt
//		texturemanager.bindTexture(swordLocation);
//		
//		tessellator.startDrawingQuads();
//		tessellator.addVertexWithUV(0.0D, 0.0D, 0.0D, (double)0, (double)1);
//		tessellator.addVertexWithUV(1.0D, 0.0D, 0.0D, (double)1, (double)1);
//		tessellator.addVertexWithUV(1.0D, 1.0D, 0.0D, (double)1, (double)0);
//		tessellator.addVertexWithUV(0.0D, 1.0D, 0.0D, (double)0, (double)0);
//		tessellator.draw();
//		
//		tessellator.startDrawingQuads();
//		tessellator.setNormal(0.0F, 0.0F, -1.0F);
//		tessellator.addVertexWithUV(0.0D, 1.0D, (double)(0.0F - 0.0625f), (double)0, (double)0);
//		tessellator.addVertexWithUV(1.0D, 1.0D, (double)(0.0F - 0.0625f), (double)1, (double)0);
//		tessellator.addVertexWithUV(1.0D, 0.0D, (double)(0.0F - 0.0625f), (double)1, (double)1);
//		tessellator.addVertexWithUV(0.0D, 0.0D, (double)(0.0F - 0.0625f), (double)0, (double)1);
//		tessellator.draw();
//		
//		float f5 = 0.5F * (0 - 1) / (float)16;
//		float f6 = 0.5F * (1 - 0) / (float)16;
//		tessellator.startDrawingQuads();
//		tessellator.setNormal(-1.0F, 0.0F, 0.0F);
//		int k;
//		float f7;
//		float f8;
//		for (k = 0; k < 16; ++k) {
//			f7 = (float)k / (float)16;
//			f8 = 0 + (1 - 0) * f7 - f5;
//			tessellator.addVertexWithUV((double)f7, 0.0D, (double)(0.0F - 0.0625f), (double)f8, (double)1);
//			tessellator.addVertexWithUV((double)f7, 0.0D, 0.0D, (double)f8, (double)1);
//			tessellator.addVertexWithUV((double)f7, 1.0D, 0.0D, (double)f8, (double)0);
//			tessellator.addVertexWithUV((double)f7, 1.0D, (double)(0.0F - 0.0625f), (double)f8, (double)0);
//		}
//
//		tessellator.draw();
//		
//		tessellator.startDrawingQuads();
//		tessellator.setNormal(1.0F, 0.0F, 0.0F);
//		float f9;
//		for (k = 0; k < 16; ++k) {
//			f7 = (float)k / (float)16;
//			f8 = 0 + (1 - 0) * f7 - f5;
//			f9 = f7 + 1.0F / (float)16;
//			tessellator.addVertexWithUV((double)f9, 1.0D, (double)(0.0F - 0.0625f), (double)f8, (double)0);
//			tessellator.addVertexWithUV((double)f9, 1.0D, 0.0D, (double)f8, (double)0);
//			tessellator.addVertexWithUV((double)f9, 0.0D, 0.0D, (double)f8, (double)1);
//			tessellator.addVertexWithUV((double)f9, 0.0D, (double)(0.0F - 0.0625f), (double)f8, (double)1);
//		}
//
//		tessellator.draw();
//		
//		tessellator.startDrawingQuads();
//		tessellator.setNormal(0.0F, 1.0F, 0.0F);
//		for (k = 0; k < 16; ++k) {
//			f7 = (float)k / (float)16;
//			f8 = 1 + (0 - 1) * f7 - f6;
//			f9 = f7 + 1.0F / (float)16;
//			tessellator.addVertexWithUV(0.0D, (double)f9, 0.0D, (double)0, (double)f8);
//			tessellator.addVertexWithUV(1.0D, (double)f9, 0.0D, (double)1, (double)f8);
//			tessellator.addVertexWithUV(1.0D, (double)f9, (double)(0.0F - 0.0625f), (double)1, (double)f8);
//			tessellator.addVertexWithUV(0.0D, (double)f9, (double)(0.0F - 0.0625f), (double)0, (double)f8);
//		}
//
//		tessellator.draw();
//		
//		tessellator.startDrawingQuads();
//		tessellator.setNormal(0.0F, -1.0F, 0.0F);
//		for (k = 0; k < 16; ++k) {
//			f7 = (float)k / (float)16;
//			f8 = 1 + (0 - 1) * f7 - f6;
//			tessellator.addVertexWithUV(1.0D, (double)f7, 0.0D, (double)1, (double)f8);
//			tessellator.addVertexWithUV(0.0D, (double)f7, 0.0D, (double)0, (double)f8);
//			tessellator.addVertexWithUV(0.0D, (double)f7, (double)(0.0F - 0.0625f), (double)0, (double)f8);
//			tessellator.addVertexWithUV(1.0D, (double)f7, (double)(0.0F - 0.0625f), (double)1, (double)f8);
//		}
//
//	  tessellator.draw();
//		
//	}
}



