package com.gekox.gekosLasers.client.renderer.tileEntity;

import org.lwjgl.opengl.GL11;

import com.gekox.gekosLasers.blocks.tileEntity.TileEntityRelay;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;
import com.gekox.gekosLasers.client.render.model.ModelRelay;
import com.gekox.gekosLasers.reference.Textures;

import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;

public class TileEntityRendererRelay extends TileEntitySpecialRenderer{

	private final ModelRelay modelRelay = new ModelRelay();
	private final RenderItem customRenderItem;
	
	public TileEntityRendererRelay() {
		
		customRenderItem = new RenderItem() {
			@Override
			public boolean shouldBob() {
				return false;
			}
		};

		customRenderItem.setRenderManager(RenderManager.instance);
	}
	
	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float tick) {
		if (tileEntity instanceof TileEntityRelay) {
			
			TileEntityRelay relay = (TileEntityRelay) tileEntity;

			GL11.glPushMatrix();

			// Scale, Translate, Rotate
			scaleTranslateRotate(x, y, z, relay.getOrientation());

			// Bind texture
			this.bindTexture(Textures.Model.RELAY);

			// Render
			modelRelay.render();

			GL11.glPopMatrix();
		}
	}

	private void scaleTranslateRotate(double x, double y, double z, ForgeDirection orientation) {
		
		if (orientation == ForgeDirection.UP) {
			GL11.glTranslated(x + 1, y + 1, z + 1);
			GL11.glRotatef(-90F, 0F, 1F, 0F);
			GL11.glRotatef(180, 0, 0, 1F);
		}
		
		else if (orientation == ForgeDirection.DOWN) {
			GL11.glTranslated(x + 1, y, z);
			GL11.glRotatef(-90F, 0F, 1F, 0F);
		}
		
		else if (orientation == ForgeDirection.NORTH) {
			GL11.glTranslated(x + 1, y, z);
			GL11.glRotatef(180F, 0F, 1F, 0F);
			GL11.glRotatef(-90F, 1F, 0F, 0F);
		}
		
		else if (orientation == ForgeDirection.EAST) {
			GL11.glTranslated(x + 1, y, z + 1);
			GL11.glRotatef(90F, 0F, 1F, 0F);
			GL11.glRotatef(-90F, 1F, 0F, 0F);
		}
		
		else if (orientation == ForgeDirection.SOUTH) {
			GL11.glTranslated(x, y, z + 1);
			GL11.glRotatef(0F, 0F, 1F, 0F);
			GL11.glRotatef(-90F, 1F, 0F, 0F);
		}
		
		else if (orientation == ForgeDirection.WEST) {
			GL11.glTranslated(x, y, z);
			GL11.glRotatef(-90F, 0F, 1F, 0F);
			GL11.glRotatef(-90F, 1F, 0F, 0F);
		}
	}
}
