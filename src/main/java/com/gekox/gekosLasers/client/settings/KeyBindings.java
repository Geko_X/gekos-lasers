package com.gekox.gekosLasers.client.settings;

import org.lwjgl.input.Keyboard;

import com.gekox.gekosLasers.reference.Names;

import net.minecraft.client.settings.KeyBinding;

// All keybindings exist here
public class KeyBindings {

	public static KeyBinding laserSize = new KeyBinding(Names.Keys.LASER_SIZE, Keyboard.KEY_F, Names.Keys.CATEGORY);
	public static KeyBinding laserDepth = new KeyBinding(Names.Keys.LASER_DEPTH, Keyboard.KEY_V, Names.Keys.CATEGORY);
	
}
