package com.gekox.gekosLasers.client.gui;

import com.gekox.gekosLasers.handler.ConfigHandler;
import com.gekox.gekosLasers.reference.Reference;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.client.config.GuiConfig;

// GUI Config handler for the mod
// https://www.youtube.com/watch?v=M-yxLJEtx7Q&index=7&list=PLQPiZYWovwmnZlgvbHCbz6TefIgeEiVcj

public class ModGuiConfig extends GuiConfig {

	public ModGuiConfig(GuiScreen guiScreen) {
		
		super(guiScreen,
				new ConfigElement(ConfigHandler.config.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(), 
				Reference.MODID,
				false,
				false,
				GuiConfig.getAbridgedConfigPath(ConfigHandler.config.toString()));
		
	}
	
}
