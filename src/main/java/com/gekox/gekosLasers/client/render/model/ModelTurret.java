package com.gekox.gekosLasers.client.render.model;

import com.gekox.gekosLasers.reference.Models;

import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelTurret {

	private IModelCustom modelTurret;
	
	public ModelTurret() {
		
		modelTurret = AdvancedModelLoader.loadModel(Models.TURRET);
	}
	
	public void render(boolean renderSphere) {
		
		modelTurret.renderPart("Base");
		
//		if(renderSphere) {
//			modelTurret.renderPart("Sphere");
//		}
	}
}
