package com.gekox.gekosLasers.reference;

// List of all keys
public enum Key {

	UNKNOWN,
	LASER_SIZE,
	LASER_DEPTH
	
}
