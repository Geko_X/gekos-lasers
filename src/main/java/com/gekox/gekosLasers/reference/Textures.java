package com.gekox.gekosLasers.reference;

import com.gekox.gekosLasers.utility.ResourceLocationHelper;

import net.minecraft.util.ResourceLocation;

public class Textures {

	public static final class Model {

		private static final String MODEL_TEXTURE_LOCATION = "textures/models/";
		
		public static final ResourceLocation TURRET = ResourceLocationHelper.getResourceLocation(MODEL_TEXTURE_LOCATION + "turret.png");
		public static final ResourceLocation TURRET_SPHERE = ResourceLocationHelper.getResourceLocation(MODEL_TEXTURE_LOCATION + "sphere.png");
		public static final ResourceLocation RELAY = ResourceLocationHelper.getResourceLocation(MODEL_TEXTURE_LOCATION + "relay.png");
		
	}
	
}
