package com.gekox.gekosLasers.reference;

import com.gekox.gekosLasers.utility.LogHelper;

// Flags for if certain mods are present

public class ModReference {

	public static boolean MOD_THERMAL_FOUNDATION = false;
	public static boolean MOD_THERMAL_EXPANSION = false;
	
	public static void listMods() {
		LogHelper.info("Listing found mods");
		LogHelper.info("Thermal Foundation: " + MOD_THERMAL_FOUNDATION);
		LogHelper.info("Thermal Expansion: " + MOD_THERMAL_EXPANSION);
	}
	
}
