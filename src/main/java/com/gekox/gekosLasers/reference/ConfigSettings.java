package com.gekox.gekosLasers.reference;

public class ConfigSettings {

	// General
	public static float LASER_SOUNDS = 0.5f;
	public static boolean EXPLOSIONS_RESPECT_GAMERULES = false;
	public static boolean TORGUE = false;
	
	// Crafting
	public static boolean USE_TE_RECIPES;
	
	// World gen
	public static boolean SPAWN_LASERITE;
	public static int LASERITE_OREGEN_CHANCE = 15;
	
	// Laser settings
	
	public static int LASER_HARDNESS;
	
	public static float RIFLE_COOLDOWN_MODIFER;
	public static float RIFLE_DAMAGE_MODIFER;
	public static float RIFLE_ENERGY_MODIFER;
	
	public static float MINING_COOLDOWN_MODIFER;
	public static float MINING_DAMAGE_MODIFER;
	public static float MINING_ENERGY_MODIFER;
	
	public static float WIDE_COOLDOWN_MODIFER;
	public static float WIDE_DAMAGE_MODIFER;
	public static float WIDE_ENERGY_MODIFER;
	
	public static float EXPLOSIVE_COOLDOWN_MODIFER;
	public static float EXPLOSIVE_DAMAGE_MODIFER;
	public static float EXPLOSIVE_ENERGY_MODIFER;
	
	public static float ENDER_COOLDOWN_MODIFER;
	public static float ENDER_DAMAGE_MODIFER;
	public static float ENDER_ENERGY_MODIFER;
	
	public static float FIRE_COOLDOWN_MODIFER;
	public static float FIRE_DAMAGE_MODIFER;
	public static float FIRE_ENERGY_MODIFER;
	
	public static float SWORD_DAMAGE_MODIFER;
	public static float SWORD_REFLECT_DAMAGE_MODIFER;
	
	public static int TIER_1_MAX_ENERGY;
	public static int TIER_1_BASE_ENERGY_USE;
	public static int TIER_1_BASE_COOLDOWN;
	public static int TIER_1_BASE_DAMAGE;
	
	public static int TIER_2_MAX_ENERGY;
	public static int TIER_2_BASE_ENERGY_USE;
	public static int TIER_2_BASE_COOLDOWN;
	public static int TIER_2_BASE_DAMAGE;
	
	public static int TIER_3_MAX_ENERGY;
	public static int TIER_3_BASE_ENERGY_USE;
	public static int TIER_3_BASE_COOLDOWN;
	public static int TIER_3_BASE_DAMAGE;
	
	public static int TIER_4_MAX_ENERGY;
	public static int TIER_4_BASE_ENERGY_USE;
	public static int TIER_4_BASE_COOLDOWN;
	public static int TIER_4_BASE_DAMAGE;
	
	public static int RELAY_UPDATE_FREQUENCY;
	public static int RELAY_RF_STORAGE;
	
//	public int laserDamage = 0;
//	public int laserStrength = -1;
//	public int laserBlockBreakRadius = 1;
//	public float laserExplosionSize = 0;
//	public int laserBurnTime = 0;
	
}
