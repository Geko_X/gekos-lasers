package com.gekox.gekosLasers.crafting;

import java.util.List;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.ItemStack;

public class ModCraftingManager {

	public static void addNBTShapelessRecipe(ItemStack output, ItemStack input, String NBTKey, int NBTValue) {
		
		NBTShapelessCrafting recipe = new NBTShapelessCrafting(output, input, NBTKey, NBTValue);
		GameRegistry.addRecipe(recipe);
	}
	
}
