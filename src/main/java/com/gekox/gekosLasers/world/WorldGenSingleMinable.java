package com.gekox.gekosLasers.world;

import java.util.Random;

import com.gekox.gekosLasers.utility.LogHelper;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

/**
 * World generation for laserite ore
 * 
 * @author Xavier Hunt
 *
 */
public class WorldGenSingleMinable extends WorldGenerator {

	private Block block;
	private int blockMeta;
	private Block target;
	
	/**
	 * Generates a block, with the given metadata, where the target block already is
	 * @param block
	 * @param meta
	 * @param target
	 */
	public WorldGenSingleMinable(Block block, int meta, Block target) {
		this.block = block;
		this.blockMeta = meta;
		this.target = target;
	}
	
	/**
	 * Generates a block, with no metadata, where the target block already is
	 * @param block
	 * @param target
	 */
	public WorldGenSingleMinable(Block block, Block target) {
		this(block, 0, target);
	}
	
	/**
	 * Generates a block, with no metadata, where smooth stone already is
	 * @param block
	 */
	public WorldGenSingleMinable(Block block) {
		this(block, Blocks.stone);
	}
	
	@Override
	public boolean generate(World world, Random rand, int x, int y, int z) {
		if (world.getBlock(x, y, z).isReplaceableOreGen(world, x, y, z, this.target)) {
			world.setBlock(x, y, z, this.block, this.blockMeta, 2);
			//LogHelper.info(String.format("Generated %s at %d, %d, %d", this.block.getUnlocalizedName(), x, y, z));
		}
		return true;
	}
	

}
