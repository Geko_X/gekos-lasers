package com.gekox.gekosLasers.items;

import java.util.List;

import com.gekox.gekosLasers.creativeTab.CreativeTabGL;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaser;
import com.gekox.gekosLasers.reference.ConfigSettings;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.Reference;
import com.gekox.gekosLasers.utility.Color;
import com.gekox.gekosLasers.utility.ColorUtils;
import com.gekox.gekosLasers.utility.LaserDamageSource;
import com.gekox.gekosLasers.utility.LaserSwordDamageSource;
import com.gekox.gekosLasers.utility.NBTHelper;
import com.gekox.gekosLasers.utility.StringUtils;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class ItemLaserSword extends ItemLaserThing {

	public int damage = (int)ConfigSettings.SWORD_DAMAGE_MODIFER;
	protected int startTime;
	
	protected IIcon[] beamIcons = new IIcon[16];
	protected IIcon hiltIcon;
	
	private final String SWORD_ACTIVE_TAG = LaserReference.SWORD_ACTIVE_TAG;
	private boolean isActive = false;
	
	private boolean isRightClicking = false;
	private boolean canToggle = true;
	
	/**
	 * Constructor. <br>
	 * 
	 */
	public ItemLaserSword(ItemStack stack) {
		super();
		this.setCreativeTab(CreativeTabGL.GL_TAB);
		this.setContainerItem(stack);
	}
	
	/**
	 * Returns the unlocalized name, in an unwrapped form
	 * @param unlocalizedName
	 * @return
	 */
	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}
	
	public boolean getIsActive(ItemStack stack) {
		//return NBTHelper.getBoolean(stack, this.SWORD_ACTIVE_TAG);
		return true;
	}
	
	public void setIsActive(ItemStack stack, boolean value) {
		//NBTHelper.setBoolean(stack, SWORD_ACTIVE_TAG, value);
		//this.isActive = value;
		this.isActive = true;
	}
	
	@Override
	public boolean onEntitySwing(EntityLivingBase entBase, ItemStack stack) {
		
		entBase.worldObj.playSoundAtEntity(entBase, Reference.RESOURCE_PREFIX + "laser.sword", 0.5f, 1);
		
		return false;
	}
	
	@Override
	public void onUpdate(ItemStack stack, World world, Entity player, int p_77663_4_, boolean par5) {
		
		// Check right click
		
		if(this.canToggle && player.isSneaking() && par5 && this.isRightClicking) {
			if(player instanceof EntityPlayerMP && ((EntityPlayerMP)player).getCurrentEquippedItem() == stack) {
				
				boolean isActive = getIsActive(stack);
				setIsActive(stack, !isActive);
				
				this.canToggle = false;
			}
		}
	}
	
//	@Override
//	public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
//		
//		if(entity instanceof EntityLiving) {
//			((EntityLiving)entity).attackEntityFrom(DamageSource.generic, this.damage);
//		}
//		
//		return false;	
//	}
	
	@Override
	public boolean hitEntity(ItemStack stack, EntityLivingBase entityTarget, EntityLivingBase entityUser) {
		
		if(entityUser instanceof EntityPlayer) {
			
			EntityPlayer player = (EntityPlayer)entityUser;
			ItemLaserThing laserThing = (ItemLaserThing)stack.getItem();
			
			int curEnergy = laserThing.getEnergyStored(stack);
			int energyPerShot = laserThing.getEnergyPerUse(stack);
			
			if(!player.capabilities.isCreativeMode) {
				useEnergy(stack, false);
			}
		
			DamageSource ds = new LaserSwordDamageSource(player);
			entityTarget.attackEntityFrom(ds, curEnergy >= energyPerShot? this.damage : 1);
			entityTarget.setFire(this.damage);
		
			return true;
		}
		
		return true;
		
	}
	
	@Override 
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b0) {
		
		String color = StringUtils.toTitleCase(ColorUtils.names[NBTHelper.getInt(stack, LaserReference.LASER_COLOR_TAG)]);
		
		String code = ColorUtils.codes[NBTHelper.getInt(stack, LaserReference.LASER_COLOR_TAG)];
		list.add(String.format("%s%s%s lasersaber", code, color, ColorUtils.CODE_LIGHT_GRAY));
		
		if(GuiScreen.isShiftKeyDown()) {
			list.add(String.format("Energy: %d/%dRF", this.getEnergyStored(stack), this.getMaxEnergyStored(stack)));
			list.add(String.format("Uses left: %d", this.getEnergyStored(stack) / this.energyPerUse));
			list.add(String.format("%d points of damage", this.damage));
			list.add(String.format("Sets things on fire for %ds", this.damage));
		}
		
		else {
			list.add(String.format("%sPress %sSHIFT%s for details", ColorUtils.CODE_LIGHT_GRAY, ColorUtils.CODE_YELLOW, ColorUtils.CODE_LIGHT_GRAY));
		}
	}
	
	/**
     * returns the action that specifies what animation to play when the items is being used
     */
	@Override
    public EnumAction getItemUseAction(ItemStack stack) {
		return EnumAction.block;
	}

	/**
	 * How long it takes to use or consume an item
	 */
	@Override
    public int getMaxItemUseDuration(ItemStack stack) {
		return 72000;
	}

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
	@Override
    public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer entityPlayer) {
		
		this.isRightClicking = true;
		
		entityPlayer.setItemInUse(stack, this.getMaxItemUseDuration(stack));
		return stack;
    }
	
	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World world, EntityPlayer entityPlayer, int ticksInUseFor) {
		
		this.canToggle = true;
		this.isRightClicking = false;
	}
	
	@Override
	public boolean shouldRotateAroundWhenRendering() {
		return false;
	}
	
	/**
	 * Registers the item's icon
	 */
	@Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconRegister) {
		for (int i = 0; i < 16; i++) {
			this.beamIcons[i] = iconRegister.registerIcon(this.getUnlocalizedName().substring(this.getUnlocalizedName().indexOf(".") + 1) + "Beam_" + ColorUtils.names[i]);
		}
		
		//this.hiltIcon = iconRegister.registerIcon(this.getUnlocalizedName().substring(this.getUnlocalizedName().indexOf(".") + 1));
	}
	
	@Override
	public IIcon getIconFromDamage(int meta) {
		
//		if(!this.isActive) {
//			return hiltIcon;
//		}
		
		meta = (meta > 16 - 1) ? 16 : ((meta < 0) ? 0 : meta);
		return this.beamIcons[meta];
	}
	
	@Override
	public IIcon getIconIndex(ItemStack stack) {
		
		boolean isActive = NBTHelper.getBoolean(stack, this.SWORD_ACTIVE_TAG);
		
//		if(!isActive) {
//			return this.hiltIcon;
//		}
		
		int color = NBTHelper.getInt(stack, LaserReference.LASER_COLOR_TAG);
		return this.beamIcons[color];
	}
	
}
