package com.gekox.gekosLasers.items;

import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.utility.Color;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemLaserReactor extends MetaItemGL {
	
	// Constructor
	public ItemLaserReactor() {
			
		super(4);
		this.setUnlocalizedName(Names.Items.LASER_REACTOR);

		for(int i = 0; i < this.getNumberOfItems(); i++) {
			this.names[i] = Names.Items.LASER_UPGRADE_NAMES[i];
		}
	}
}
