package com.gekox.gekosLasers.items;

import java.util.List;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

import com.gekox.gekosLasers.creativeTab.CreativeTabGL;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.Reference;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * Meta item base class
 * @author Xavier Hunt
 *
 */

public class MetaItemGL extends Item {
	
	private IIcon[] icons;
	private int numOfItems;
	/** Names to append to the end of the meta item */
	public String[] names = {"0",
							  "1",
							  "2",
							  "3",
							  "4",
							  "5",
							  "6",
							  "7",
							  "8",
							  "9",
							  "10",
							  "11",
							  "12",
							  "13",
							  "14",
							  "15"};
	
	/**
	 * Constructor. <br>
	 * Sets a new meta item with a full 16 subitems
	 * 
	 */
	public MetaItemGL() {
		this(16);
	}
	
	/**
	 * Constructor
	 * @param items: the number of subitems
	 */
	public MetaItemGL(int items) {
		super();
		icons = new IIcon[items];
		this.numOfItems = icons.length;
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		this.setCreativeTab(CreativeTabGL.GL_TAB);
	}
	
	public int getNumberOfItems() {
		return this.numOfItems;
	}
	
	/**
	 * Gets the unlocalized name
	 */
	@Override
	public String getUnlocalizedName(ItemStack stack) {
		return String.format("item.%s%s_%s", Reference.RESOURCE_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()), names[stack.getItemDamage()]);
	}
	
	public String getUnlocalizedName(int meta) {
		return String.format("item.%s%s_%s", Reference.RESOURCE_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()), names[meta]);
	}
	
	/**
	 * Returns the unlocalized name, in an unwrapped form
	 * @param unlocalizedName
	 * @return
	 */
	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}
	
	
	@Override
	public IIcon getIconFromDamage(int meta) {
		meta = (meta > numOfItems - 1) ? numOfItems : ((meta < 0) ? 0 : meta);
		return this.icons[meta];
	}
	
	@Override
	public void getSubItems(Item item, CreativeTabs tab, List list) {
		for (int i = 0; i < numOfItems; i ++) {
	    	list.add(new ItemStack(item, 1, i));
	    }
	}
	
	/**
	 * Registers the item's icon
	 */
	@Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconRegister) {
		for (int i = 0; i < numOfItems; i++) {
			this.icons[i] = iconRegister.registerIcon(this.getUnlocalizedName(i).substring(this.getUnlocalizedName(i).indexOf(".") + 1));
		}
	}
}
