package com.gekox.gekosLasers.items.lasers;

import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.items.ItemLaserGun;
import com.gekox.gekosLasers.reference.ConfigSettings;
import com.gekox.gekosLasers.reference.Names;

public class ItemTier2Laser extends ItemLaserGun {

	public ItemTier2Laser(ItemStack container, String name) {
		super();
		
		this.itemStackContainer = container;
		this.setUnlocalizedName(name);
		
		this.setNumOfThingsBroken(2);
		
		this.maxEnergy = ConfigSettings.TIER_2_MAX_ENERGY;
		this.maxTransfer = 1000;
		this.setLaserDamage(ConfigSettings.TIER_2_BASE_DAMAGE);
		this.energyPerUse = ConfigSettings.TIER_2_BASE_ENERGY_USE;
		this.setCoolDown(ConfigSettings.TIER_2_BASE_COOLDOWN);
	}
	
}
