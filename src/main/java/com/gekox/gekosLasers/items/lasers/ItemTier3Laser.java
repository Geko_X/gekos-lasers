package com.gekox.gekosLasers.items.lasers;

import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.items.ItemLaserGun;
import com.gekox.gekosLasers.reference.ConfigSettings;
import com.gekox.gekosLasers.reference.Names;

public class ItemTier3Laser extends ItemLaserGun {

	public ItemTier3Laser(ItemStack container, String name) {
		super();
		
		this.itemStackContainer = container;
		this.setUnlocalizedName(name);
		
		this.setNumOfThingsBroken(4);
		
		this.maxEnergy = ConfigSettings.TIER_3_MAX_ENERGY;
		this.maxTransfer = 1000;
		this.setLaserDamage(ConfigSettings.TIER_3_BASE_DAMAGE);
		this.energyPerUse = ConfigSettings.TIER_3_BASE_ENERGY_USE;
		this.setCoolDown(ConfigSettings.TIER_3_BASE_COOLDOWN);
	}
	
}
