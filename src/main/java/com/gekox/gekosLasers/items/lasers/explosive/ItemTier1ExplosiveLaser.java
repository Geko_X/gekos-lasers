package com.gekox.gekosLasers.items.lasers.explosive;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.init.ModItems;
import com.gekox.gekosLasers.items.ItemLaserGun;
import com.gekox.gekosLasers.items.lasers.ItemTier1Laser;
import com.gekox.gekosLasers.reference.ConfigSettings;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.Names;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemTier1ExplosiveLaser extends ItemTier1Laser {
	
	private static String unLocName = Names.Items.LASER_UPGRADE_NAMES[0] + "_" + Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[3];
	
	// Constructor
	public ItemTier1ExplosiveLaser() {
		this(new ItemStack(ModItems.itemTier1LaserCasing, 1, 3));
	}
	
	public ItemTier1ExplosiveLaser(ItemStack container) {
		
		super(container, unLocName);
		
		this.setLaserDoExplosionDamage(true);
		this.setLaserExplosionSize(4);
		
		this.setLaserDamage((int) (this.getLaserDamage() * ConfigSettings.EXPLOSIVE_DAMAGE_MODIFER));
		this.setCoolDown((int) (this.getCoolDown() * ConfigSettings.EXPLOSIVE_COOLDOWN_MODIFER));
		this.energyPerUse *= ConfigSettings.EXPLOSIVE_ENERGY_MODIFER;
	}
	
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b0) {
		list.add(LaserReference.FLAVOR_EXPLOSIVE);
		super.addInformation(stack, player, list, b0);
	}
}
