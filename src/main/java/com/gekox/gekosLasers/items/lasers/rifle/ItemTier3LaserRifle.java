package com.gekox.gekosLasers.items.lasers.rifle;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.init.ModItems;
import com.gekox.gekosLasers.items.lasers.ItemTier3Laser;
import com.gekox.gekosLasers.reference.ConfigSettings;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.Names;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemTier3LaserRifle extends ItemTier3Laser {
	
	private static String unLocName = Names.Items.LASER_UPGRADE_NAMES[2] + "_" + Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[0];
	
	// Constructor
	public ItemTier3LaserRifle() {
		this(new ItemStack(ModItems.itemTier3LaserCasing, 1, 0));
	}
	
	public ItemTier3LaserRifle(ItemStack container) {
		
		super(container, unLocName);
		
		this.setLaserDamage((int) (this.getLaserDamage() * ConfigSettings.RIFLE_DAMAGE_MODIFER));
		this.setCoolDown((int) (this.getCoolDown() * ConfigSettings.RIFLE_COOLDOWN_MODIFER));
		this.energyPerUse *= ConfigSettings.RIFLE_ENERGY_MODIFER;
	}
	
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b0) {
		list.add(LaserReference.FLAVOR_RIFLE);
		super.addInformation(stack, player, list, b0);
	}
}
