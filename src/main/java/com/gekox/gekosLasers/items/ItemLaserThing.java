package com.gekox.gekosLasers.items;

import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;

import com.gekox.gekosLasers.entity.projectile.laser.*;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.utility.Color;
import com.gekox.gekosLasers.utility.LogHelper;
import com.gekox.gekosLasers.utility.NBTHelper;
import com.gekox.gekosLasers.utility.StringUtils;

import cofh.api.energy.IEnergyContainerItem;
import cofh.lib.util.helpers.EnergyHelper;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public abstract class ItemLaserThing extends ItemGL implements IEnergyContainerItem  {

	protected int maxEnergy = 100000;
	public int maxTransfer = 1000;
	public int energyPerUse = 200;
	
	private int coolDown = 20;
	protected int currentCooldown = 0;
	protected long nextShot = 0;
	
	public final String ENERGY_TAG = "Energy";
//	public final String COOLDOWN_TAG = "Cooldown";
	public final String NEXT_USE_TAG = "NextUse";
	
	private final String COLOR_TAG = LaserReference.LASER_COLOR_TAG;
	
	protected ItemStack itemStackContainer;
	
	public ItemLaserThing() {
		this.setMaxStackSize(1);
	}
	
	public ItemLaserThing setEnergyParams(int maxEnergy, int maxTransfer, int energyPerShot) {
		
		this.maxEnergy = maxEnergy;
		this.maxTransfer = maxTransfer;
		this.energyPerUse = energyPerShot;
		
		return this;
	}
	
	/**
	 * Gets the ItemLaserThing that is associated with an ItemStack
	 */
	public ItemLaserThing getLaserThing(ItemStack stack) {
		Item item = stack.getItem();
		
		if(item instanceof ItemLaserThing) {
			return (ItemLaserThing)item;
		}
		
		return null;
	}
	
	public Color getColor(ItemStack stack) {
		return Color.color[NBTHelper.getInt(stack, COLOR_TAG)];
	}
	
	public int getColorID(ItemStack stack) {
		return NBTHelper.getInt(stack, COLOR_TAG);
	}
	
	/**
	 * Returns true if the player can shoot the laser thing
	 * @param stack
	 * @return If the item can fire
	 */
	
	public boolean canFire(ItemStack stack, World world) {
		
		ItemLaserThing laserThing = getLaserThing(stack);
		if(laserThing == null)
			return false;	

		if(laserThing.getEnergyStored(stack) < laserThing.getEnergyPerUse(stack)) {
			return false;
		}
		
		if(world.isRemote) {
		
			if(Minecraft.getSystemTime() < this.getNextShot(stack)) {
				return false;
			}
		}
		
		else {
			if(MinecraftServer.getSystemTimeMillis() < this.getNextShot(stack)) {
				return false;
			}
		}
		
//		if(laserThing.getCurrentCooldown(stack) > laserThing.getCoolDown(stack)) {
//			return false;
//		}
//		
		return true;
	}
	
	public int getCoolDown() {
		//return NBTHelper.getInt(stack, COOLDOWN_TAG);
		return this.coolDown;
	}

	public void setCoolDown(int coolDown) {
		this.coolDown = coolDown;
	}
	
	public long getNextShot(ItemStack stack) {
		return NBTHelper.getLong(stack, NEXT_USE_TAG);
	}

	/**
	 * Sets the cooldown of the laser thing
	 * @param stack
	 * @param coolDown
	 */
	public void setCooldown(ItemStack stack, int coolDown) {
		ItemLaserThing laserThing = getLaserThing(stack);
		laserThing.setCoolDown(coolDown);
	}
	
	/**
	 * Gets the current time left before this laser thing can fire
	 * @param stack
	 * @return The current cooldown
	 */
	public int getCurrentCooldown(ItemStack stack) {
		//ItemLaserThing laserThing = getLaserThing(stack);
		//return (laserThing == null) ? null : laserThing.currentCooldown;
		return NBTHelper.getInt(stack, NEXT_USE_TAG);
	}
	
	/**
	 * Call this to update when the next shot should happen. <br>
	 * This is called automatically after firing
	 * @param stack
	 */
	public void setNextShot(ItemStack stack, World world) {
		//ItemLaserThing laserThing = getLaserThing(stack);
		//this.currentCooldown += this.getCoolDown();
		if(world.isRemote)
			NBTHelper.setLong(stack, NEXT_USE_TAG, Minecraft.getSystemTime() + (this.getCoolDown() * 20));
		else {
			NBTHelper.setLong(stack, NEXT_USE_TAG, MinecraftServer.getSystemTimeMillis() + (this.getCoolDown() * 20));
		}
		
	//	LogHelper.info("Setting next shot to: " + NBTHelper.getLong(stack, NEXT_USE_TAG));
		
		//NBTHelper.setInteger(stack, NEXT_USE_TAG, this.getCoolDown(stack));
		
	}
	
	@Override
	public void onUpdate(ItemStack item, World world, Entity entity, int i0, boolean b0) {
		
		super.onUpdate(item, world, entity, i0, b0);
		
//		if(item.getItem() instanceof ItemLaserGun) {
//			
//			if(currentCooldown >= 0) {
//				currentCooldown--;
//				NBTHelper.setInteger(item, COOLDOWN_TAG, currentCooldown);
//			}
//		}
		
	//	LogHelper.info("Cooldown info: " + currentCooldown + " : " + item);

	}
	
	/* Energy things */
	
	protected int useEnergy(ItemStack stack, boolean simulate) {
		
		// Check for efficiency enchant?
		return extractEnergy(stack, energyPerUse, simulate);
		
	}
	
	public int getEnergyPerUse(ItemStack stack) {
		return energyPerUse;
		
	}
	
	/* Item stuff */
	
	@Override
	public boolean getIsRepairable(ItemStack itemToRepair, ItemStack stack) {

		return false;
	}
	
	@Override
	public int getDisplayDamage(ItemStack stack) {

		if (stack.stackTagCompound == null) {
			EnergyHelper.setDefaultEnergyTag(stack, 0);
		}
		return maxEnergy - stack.stackTagCompound.getInteger(ENERGY_TAG);
	}
	
	@Override
	public int getMaxDamage(ItemStack stack) {
		return maxEnergy;
	}
	
	@Override
	public boolean isDamaged(ItemStack stack) {
		return true;
	}
	
	@SideOnly(Side.CLIENT)
	public abstract void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b0);

	@Override
	public boolean doesContainerItemLeaveCraftingGrid(ItemStack itemStack) {
		return false;
	}
	
	public Item setContainerItem(ItemStack stack) {
		this.itemStackContainer = stack;
		return this;
	}
	
	@Override
	public ItemStack getContainerItem(ItemStack stack) {
		return this.itemStackContainer;
	}
	
	@Override
	public boolean hasContainerItem() {
		return true;
	}
	
	/* IEnergyContainerItem */
	
	@Override
	public int receiveEnergy(ItemStack container, int maxReceive, boolean simulate) {
		if (container.stackTagCompound == null) {
			EnergyHelper.setDefaultEnergyTag(container, 0);
		}
		int stored = container.stackTagCompound.getInteger(ENERGY_TAG);
		int receive = Math.min(maxReceive, Math.min(maxEnergy - stored, maxTransfer));

		if (!simulate) {
			stored += receive;
			container.stackTagCompound.setInteger(ENERGY_TAG, stored);
		}
		
		return receive;
	}

	public int extractEnergy(ItemStack container, int maxExtract, boolean simulate) {

		if (container.stackTagCompound == null) {
			EnergyHelper.setDefaultEnergyTag(container, 0);
		}
		int stored = container.stackTagCompound.getInteger(ENERGY_TAG);
		int extract = Math.min(maxExtract, stored);

		if (!simulate) {
			stored -= extract;
			container.stackTagCompound.setInteger(ENERGY_TAG, stored);
		}
		
		return extract;
	}

	@Override
	public int getEnergyStored(ItemStack container) {
		
		if (container.stackTagCompound == null) {
			EnergyHelper.setDefaultEnergyTag(container, 0);
		}
		
		return container.stackTagCompound.getInteger(ENERGY_TAG);
	}

	@Override
	public int getMaxEnergyStored(ItemStack container) {
		return maxEnergy;
	}
	
	//====================
	
}
