package com.gekox.gekosLasers.items;

import com.gekox.gekosLasers.reference.Reference;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlockWithMetadata;
import net.minecraft.item.ItemStack;

public class ItemMetaBlockGL extends ItemBlockWithMetadata {

	public ItemMetaBlockGL(Block block) {
		super(block, block);
	}
	
	/**
	 * Gets the unlocalized name
	 */
	@Override
	public String getUnlocalizedName(ItemStack stack) {
		return String.format("tile.%s%s_%s", Reference.RESOURCE_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()), stack.getItemDamage());
	}
	
	/**
	 * Returns the unlocalized name, in an unwrapped form
	 * @param unlocalizedName
	 * @return
	 */
	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}
	
}
