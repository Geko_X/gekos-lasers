package com.gekox.gekosLasers.items;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.utility.Color;
import com.gekox.gekosLasers.utility.ColorUtils;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * Polished laserite. <p>
 * This is used as a focus for laser weapons. <br>
 * Different colors are used to change the color of the laser
 * 
 * @author Xavier Hunt
 *
 */

public class ItemRefinedLaserite extends MetaItemGL {

	public ItemRefinedLaserite() {
		super();
		this.setUnlocalizedName(Names.Items.REFINED_LASERITE);
		
		for(int i = 0; i < this.getNumberOfItems(); i++) {
			this.names[i] = ColorUtils.names[i];
		}
	}
	
	@SideOnly(Side.CLIENT)
	public boolean hasEffect(ItemStack par1ItemStack, int i) {
		return true;
	}
}
