package com.gekox.gekosLasers.items;

import com.gekox.gekosLasers.reference.Reference;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class LaserFocus extends Item {

	public LaserFocus(String unLocName) {
		
		super();
		
		this.setUnlocalizedName(unLocName);
		this.setCreativeTab(CreativeTabs.tabMaterials);
		this.setTextureName(Reference.MODID + ":" + unLocName);
		
	}

}
