package com.gekox.gekosLasers.items.cases;

import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.items.MetaItemGL;
import com.gekox.gekosLasers.reference.Names;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * Casings for normal lasers
 * 
 * @author X.Hunt
 *
 */

public class ItemTier3LaserCasing extends MetaItemGL {

	public ItemTier3LaserCasing() {
		super(Names.Items.LASER_TYPES.length);
		this.setUnlocalizedName(Names.Items.LASER_UPGRADE_NAMES[2] + "_" + Names.Items.CASE_BASENAME);
		
		for(int i = 0; i < this.getNumberOfItems(); i++) {
			this.names[i] = Names.Items.LASER_TYPES[i];
		}
	}
}